#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Error {
    NormalForm(VerifyError),
    NotMonotone,
    Unknown,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum VerifyError {
    AtomsOfDefWrong,
    OpenIsDefined,
    BadSign,
    NotAncestor,
    IndexOutOfBounds,
    CyclicNesting,
    OpenBadEnd,
    Unsorted,
}
impl From<VerifyError> for Error {
    fn from(e: VerifyError) -> Error {
        Error::NormalForm(e)
    }
}
impl From<()> for Error {
    fn from(_: ()) -> Error {
        Error::Unknown
    }
}
