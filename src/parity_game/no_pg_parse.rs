use super::{ParityGame, Player, Priority};
use data::{Atom, Body, DefIdx, NestedDefinition, Rule};
use std::collections::HashMap;
use std::io::Read;
use util::remapper::Remapper;
use util::sign::Sign;

pub struct Reader<T: Read> {
    read: T,
    buffer: [u8; 1024],
    idx: usize,
    len: usize,
}

impl<T: Read> Reader<T> {
    #[inline(always)]
    pub fn is_digit(c: u8) -> bool {
        b'0' <= c && c <= b'9'
    }
    #[inline(always)]
    pub fn is_whitespace(c: u8) -> bool {
        c == b' ' || c == b'\r' || c == b'\n' || c == b'\t'
    }
    pub fn new(t: T) -> Reader<T> {
        Reader {
            read: t,
            buffer: [0; 1024],
            idx: 0,
            len: 0,
        }
    }
    #[inline(always)]
    fn fill_buffer(&mut self) -> bool {
        if self.idx == self.len {
            let res = self.read.read(&mut self.buffer);
            if let Ok(len) = res {
                self.len = len;
                self.idx = 0;
            } else {
                self.len = 0;
                self.idx = 1;

            }
        }
        if self.idx >= self.len {
            return false;
        }
        return true;
    }
    #[inline(always)]
    pub fn peek(&mut self) -> Option<u8> {
        if !self.fill_buffer() {
            return None;
        }
        Some(self.buffer[self.idx])
    }
    #[inline(always)]
    pub fn next(&mut self) -> Option<u8> {
        if !self.fill_buffer() {
            return None;
        }
        let o = self.buffer[self.idx];
        self.idx += 1;
        Some(o)
    }
    #[inline(always)]
    pub fn read_number(&mut self) -> usize {
        let mut out: usize = 0;
        loop {
            match self.peek() {
                Some(c) if Self::is_digit(c) => out = out * 10 + (c - b'0') as usize,
                None | Some(_) => return out,
            }
            self.next();
        }
    }
    #[inline(always)]
    pub fn ws_number(&mut self) -> usize {
        self.read_whitespace();
        self.read_number()
    }
    #[inline(always)]
    pub fn non_ws_non_number(&mut self) -> Vec<u8> {
        let mut string = Vec::default();
        while let Some(c) = self.peek() {
            if Self::is_whitespace(c) || Self::is_digit(c) {
                return string;
            }
            string.push(c);
            self.next();
        }
        string
    }
    #[inline(always)]
    pub fn ws_then_non_ws_non_number(&mut self) -> Vec<u8> {
        self.read_whitespace();
        self.non_ws_non_number()
    }
    #[inline(always)]
    pub fn read_non_whitespace(&mut self) -> Vec<u8> {
        let mut string = Vec::default();
        while let Some(c) = self.peek() {
            if Self::is_whitespace(c) {
                return string;
            }
            string.push(c);
            self.next();
        }
        string
    }
    #[inline(always)]
    pub fn read_quote(&mut self) -> Vec<u8> {
        let mut string = Vec::default();
        while let Some(c) = self.peek() {
            if c == b'"' {
                return string;
            }
            string.push(c);
            self.next();
        }
        string
    }
    #[inline(always)]
    pub fn read_whitespace(&mut self) {
        while let Some(c) = self.peek() {
            if !Self::is_whitespace(c) {
                return;
            }
            self.next();
        }
    }
}

impl ParityGame {
    fn parse_definition<T: Read>(r: &mut Reader<T>) -> (NestedDefinition, Remapper<usize>) {
        let header = r.ws_then_non_ws_non_number();
        if header != b"parity" {
            panic!("No parity game header: {:?}", header);
        }
        let length = r.ws_number();
        assert!(r.ws_then_non_ws_non_number() == b";");
        let mut def = NestedDefinition::default();
        let mut priorities = Remapper::<usize>::default();

        unsafe {
            r.read_whitespace();
            if let None = r.peek() {
                return (def, priorities);
            }
            def.set_rule(Atom::new(length), Rule::default());
            loop {
                let node = Atom::new(r.ws_number());
                let prio = r.ws_number();
                let prio = Priority::from(prio);
                let owner = Player::from(r.ws_number());
                let mut edges = vec![Atom::new(r.ws_number()).pos()];
                loop {
                    r.read_whitespace();
                    match r.next() {
                        Some(b',') => {
                            edges.push(Atom::new(r.ws_number()).pos());
                        }
                        Some(b';') => break,
                        Some(b'\"') => {
                            r.read_quote();
                            assert_eq!(r.next().unwrap(), b'"');
                        }
                        Some(x) => panic!("Expected '\"',',' or ';': {}", x),
                        None => return (def, priorities),
                    }
                }
                edges.sort();
                edges.dedup();
                let body = if owner.is_odd() == prio.winner().is_odd() {
                    Body::And(edges)
                } else {
                    Body::Or(edges)
                };
                let def_idx = DefIdx::new(priorities.insert(prio.0));
                let rule = Rule { body, def: def_idx };
                def.set_rule(node, rule);
            }
        }
    }
    pub fn parse<T: Read>(r: &mut Reader<T>) -> NestedDefinition {
        let (mut def, remapper) = Self::parse_definition(r);
        let mut priorities = remapper.iter().cloned().collect::<Vec<_>>();
        priorities.sort();
        priorities.reverse();
        let mut signs = priorities
            .iter()
            .map(|x| Sign::from(Player::from(*x).is_odd()))
            .collect::<Vec<_>>();
        let nb_priorities = priorities.len();
        let priorities = priorities
            .into_iter()
            .enumerate()
            .map(|(x, y)| (y, x + 1))
            .collect::<HashMap<usize, usize>>();
        let mut parents = priorities
            .iter()
            .enumerate()
            .map(|(i, _)| DefIdx::new(i))
            .collect::<Vec<_>>();
        parents.insert(0, DefIdx::open());
        signs.insert(0, Sign::Pos);
        if def.rules.last().map(Rule::is_open).unwrap_or(false) {
            def.rules.pop();
        }
        for rule in def.rules.iter_mut() {
            let orig_prio = remapper.invert(rule.def.i()).unwrap();
            if rule.is_open() {
                rule.body = Body::And(vec![]);
                rule.def = DefIdx::new(nb_priorities);
            } else {
                rule.def = DefIdx::new(priorities[orig_prio]);
            }
        }
        def.parent = parents;
        def.signs = signs;
        def.fix_atoms_of_def();
        def.fix_signs()
    }
}
