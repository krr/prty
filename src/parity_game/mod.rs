pub mod from_mu;
pub mod to_mu;
pub mod no_pg_parse;

use util::sign::Sign;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Default)]
pub struct Player(bool);
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Default)]
pub struct Priority(usize);
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum GameError {
    EdgeOutOfBounds,
    UnequalLength,
}

#[derive(Default, Debug, Eq, PartialEq)]
pub struct ParityGame {
    edges: Vec<Vec<usize>>,
    owner: Vec<Player>,
    priority: Vec<Priority>,
    names: Vec<Option<String>>,
}

#[derive(Default, Debug, Eq, PartialEq)]
pub struct ParitySolution {
    winner: Vec<Player>,
    play: Vec<Option<usize>>,
    priority: Vec<Priority>,
}

impl ParityGame {
    fn nodes(&self) -> Box<Iterator<Item = usize>> {
        Box::new((0..self.edges.len()).into_iter())
    }
    pub fn nb_nodes(&self) -> usize {
        self.edges.len()
    }
    pub fn nb_edges(&self) -> usize {
        self.edges.iter().map(|i| i.len()).sum()
    }
    fn add_rule(&mut self, node: usize, prio: Priority, owner: Player, mut edges: Vec<usize>, name: Option<String>) {
        use util::vec;
        edges.sort();
        edges.dedup();
        vec::set_at(&mut self.names, node, name);
        vec::set_at(&mut self.edges, node, edges);
        vec::set_at(&mut self.owner, node, owner);
        vec::set_at(&mut self.priority, node, prio);
    }
    pub fn verify(&self) -> Result<(), GameError> {
        let len = self.edges.len();
        for node in self.edges.iter().flat_map(|e| e.iter()) {
            if *node >= len {
                return Err(GameError::EdgeOutOfBounds);
            }
        }
        if self.names.len() != len {
            return Err(GameError::UnequalLength);
        } else if self.owner.len() != len {
            return Err(GameError::UnequalLength);
        } else if self.priority.len() != len {
            return Err(GameError::UnequalLength);
        }
        Ok(())
    }
}
impl Player {
    pub fn is_odd(&self) -> bool {
        self.0
    }
    pub fn is_even(&self) -> bool {
        !self.is_odd()
    }
    pub fn as_number(&self) -> usize {
        if self.is_odd() {
            1
        } else {
            0
        }
    }
}
impl Priority {
    pub fn winner(&self) -> Player {
        Player(self.0 & 1 == 1)
    }
}
impl ::std::ops::Sub for Priority {
    type Output = usize;
    fn sub(self, p: Priority) -> usize {
        self.0 - p.0
    }
}
impl From<usize> for Priority {
    fn from(u: usize) -> Self {
        Priority(u)
    }
}
impl From<bool> for Player {
    fn from(b: bool) -> Self {
        Player(b)
    }
}
impl From<usize> for Player {
    fn from(u: usize) -> Self {
        Priority::from(u).winner()
    }
}
impl From<Player> for Sign {
    fn from(p: Player) -> Sign {
        p.0.into()
    }
}
impl From<Sign> for Player {
    fn from(s: Sign) -> Player {
        Player::from(bool::from(s))
    }
}

mod fmt {
    use super::{ParitySolution, Priority};
    use std::fmt::*;
    impl Display for Priority {
        fn fmt(&self, f: &mut Formatter) -> Result {
            write!(f, "{}", self.0)
        }
    }

    impl Display for ParitySolution {
        fn fmt(&self, f: &mut Formatter) -> Result {
            writeln!(f, "paritysol {};", self.winner.len()-1)?;
            for (idx, (winner, play)) in self.winner.iter().zip(self.play.iter()).enumerate() {
                let winner = winner.as_number();
                if let Some(play) = play {
                    writeln!(f, "{} {} {};", idx, winner, play)?;
                } else {
                    writeln!(f, "{} {};", idx, winner)?
                }
            }
            Ok(())
        }
    }
}

pub mod parse {
    include!(concat!(env!("OUT_DIR"), "/parity_game.rs"));

    #[test]
    fn parse() {
        assert_eq!(
            line("0 7 3 4,5,6 \"Hello\""),
            Ok((0, Priority(7), Player(true), vec![4, 5, 6], Some("Hello".into())))
        );
        let g1 = "parity 4;1 3 0 0,1,3,4 \"Europe\";0 6 1 4,2 \"Africa\";4 5 1 0 \"Antarctica\";1 8 1 2,4,3 \"America\";3 6 0 4,2 \"Australia\";2 7 0 3,1,0,4 \"Asia\";";
        let g2 =
            "4 5 1 0,0,0 \"Antarctica\";1 8 1 3,4,2 \"America\";3 6 0 2,4 \"Australia\";2 7 0 0,1,3,1,0,4 \"Asia\";0 6 1 4,2 \"Africa\";";
        assert_eq!(game(g1), game(g2));
        assert_eq!(game(g1).unwrap().verify().unwrap(), ());
    }
}
