use super::ParityGame;
use data::*;
use std::default::Default;

impl ParityGame {
    pub fn as_nested_def_inline(&self) -> NestedDefinition {
        let mut priority_map = self.priority.clone();
        priority_map.sort();
        priority_map.dedup();
        let mut def = NestedDefinition::default();
        unsafe {
            def.set_rule(Atom::new(self.nb_nodes() - 1), Rule::default());
            priority_map.reverse();
            let mut definitions = priority_map
                .iter()
                .scan(DefIdx::open(), |prev_def, p| {
                    *prev_def = def.add_definition(*prev_def, p.winner().into()).unwrap();
                    Some(*prev_def)
                })
                .collect::<Vec<DefIdx>>();
            priority_map.reverse();
            definitions.reverse();
            for (i, edges) in self.edges.iter().enumerate() {
                let factlist = edges
                    .iter()
                    .map(|&to| Atom::new(to).pos())
                    .collect::<Vec<_>>();
                let body = if self.owner[i].is_odd() == self.priority[i].winner().is_odd() {
                    Body::And(factlist)
                } else {
                    Body::Or(factlist)
                };
                let prio_from = self.priority[i];
                let depth_from = priority_map.binary_search(&prio_from).unwrap();
                let def_from = definitions[depth_from];
                let rule = Rule {
                    body,
                    def: def_from,
                };
                def.set_rule(Atom::new(i), rule);
            }
        }
        def.fix_atoms_of_def();
        def.fix_signs()
    }
}
