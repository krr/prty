use super::{ParityGame, ParitySolution, Player, Priority};
use bitgraph::Graph;
use data::interpretation::Eval;
use data::{Atom, Loc, NestedDefinition, Sign};
use state::State;
use util::permutation::Permutation;

impl ParityGame {
    pub fn from_mu(&self, inter: &State, p: &Permutation) -> ParitySolution {
        let invert = p.invert();
        let mut winners = vec![];
        let mut plays: Vec<Option<usize>> = vec![];
        for node in self.nodes() {
            let atom: Atom = p.permute(&Atom::new(node));
            let eval = inter.eval(&atom).is_true();
            let owner = self.owner[node];
            let winner = Player::from(eval != self.priority[node].winner().is_odd());
            let play = if winner == owner {
                // choose
                let mut iter = inter.graph.iter_to(atom);
                let atom = invert.permute(&iter.next().unwrap());
                assert!(iter.next().is_none());
                Some(atom.idx())
            } else {
                None
            };
            winners.push(winner);
            plays.push(play);
        }
        ParitySolution {
            winner: winners,
            play: plays,
            priority: self.priority.clone(),
        }
    }
    pub fn from_mu_state(def: &NestedDefinition, inter: &State, p: &Permutation) -> ParitySolution {
        let invert = p.invert();
        let mut winners = vec![];
        let mut plays: Vec<Option<usize>> = vec![];
        let mut priorities = vec![];
        let offset: isize = if def.signs[def.top().i()] == Sign::Pos { 0 } else { 1 };
        let depth = def.signs.len() as isize - 1;
        for node in def.atoms() {
            let atom = p.permute(&node);
            let player_dominion = def.sign(&atom);
            let priority = depth - atom.loc(def).i() as isize + offset;
            let relative_player = def.rule(&atom).is_or();
            let flip_absolute_player = Sign::Pos == player_dominion;
            let relative_player_wins = inter.eval(&atom).is_true() == relative_player;
            let play = if relative_player_wins {
                let mut iter = inter.graph.iter_to(atom);
                let atom = invert.permute(&iter.next().unwrap());
                Some(atom.idx())
            } else {
                None
            };
            plays.push(play);
            priorities.push(Priority::from(priority as usize));
            winners.push(Player::from(inter.eval(&atom).is_true() == flip_absolute_player));
        }
        ParitySolution {
            winner: winners,
            play: plays,
            priority: priorities,
        }
    }
}
