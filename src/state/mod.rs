pub mod induction;
pub mod queue;
pub mod rule;
pub mod stat;

use bitgraph::{MapGraph, SparseGraph};
pub type JustGraph = MapGraph<SparseGraph, Atom>;
use self::queue::Queue;
pub use self::rule::Rule;
use self::stat::Stat;
use util::bitvec::BitVec;
use data::interpretation::Inter;
use data::{Atom, DefInter, Fact, NestedDefinition};

impl DefInter {
    pub fn induct(&self, opt: bool) -> State {
        let DefInter(ref def, ref inter) = *self;
        trace!("{:?}", def.atoms_of);
        let mut state = State::new_inter(&def, inter.clone());
        state.opt = opt;
        state.reset(&def);
        state.induct_generic(&def, State::induct_just_step);
        state
    }
}
pub struct State {
    pub graph: JustGraph,
    pub is_justified: BitVec,
    pub interpretation: Inter,
    pub queue: Queue,
    pub other_queue: Vec<Atom>,
    pub stat: Stat,
    pub opt: bool,
}
impl From<Fact> for usize {
    fn from(f: Fact) -> usize {
        (&f).into()
    }
}
impl<'a> From<&'a Fact> for usize {
    fn from(f: &'a Fact) -> usize {
        ((f.atom().idx()) << 1) + f.sign().value()
    }
}
impl From<usize> for Fact {
    fn from(f: usize) -> Fact {
        let sign = ::util::sign::Sign::from_value(f & 1);
        Fact::new(sign, Atom::new(f / 2))
    }
}
impl State {
    pub fn new(def: &NestedDefinition) -> State {
        let interpretation = Inter::new(def.nb_atoms());
        State::new_inter(def, interpretation)
    }
    pub fn new_inter(def: &NestedDefinition, interpretation: Inter) -> State {
        let size = def.nb_atoms();
        let graph = JustGraph::from(SparseGraph::new(size)).into();
        let queue = Queue::build(def);
        State {
            graph,
            is_justified: BitVec::with_capacity(size),
            interpretation,
            queue,
            other_queue: vec![],
            opt: false,
            stat: Default::default(),
        }
    }
    pub fn done(&self) -> bool {
        self.queue.is_empty()
    }
    pub fn is_justified(&self, a: &Atom) -> bool {
        self.is_justified.get(usize::from(a))
    }
    pub fn set_justified(&mut self, a: Atom, b: bool) -> bool {
        self.is_justified.set(usize::from(a), b)
    }
}
use data::interpretation::*;
impl Eval<Atom> for State {
    type W = <Inter as Eval<Atom>>::W;
    fn eval(&self, a: &Atom) -> Witness<()> {
        self.interpretation.eval(a)
    }
}
