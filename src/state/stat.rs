#[derive(Default, Debug, Copy, Clone, Eq, PartialEq)]
pub struct Stat {
    pub steps: usize,
    pub prop: usize,
    pub prop_false: usize,
    pub prop_atoms: usize,
    pub prop_just_size: usize,
    pub prop_subscribe: usize,
    pub prop_sub_count: usize,
    pub unsupported: usize,
    pub unfounded: usize,
    pub unfounded_atoms: usize,
    pub unfounded_just_size: usize,
    pub prop_wake: usize,
    pub unsupported_wake: usize,
    pub clear_prop: usize,
}

use std::fmt::*;

impl Display for Stat {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(
            f,
            "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}",
            self.steps,
            self.prop,
            self.prop_false,
            self.prop_atoms,
            self.prop_just_size,
            self.prop_subscribe,
            self.prop_sub_count,
            self.unsupported,
            self.unfounded,
            self.unfounded_atoms,
            self.unfounded_just_size,
            self.prop_wake,
            self.unsupported_wake,
            self.unsupported - self.prop_wake,
            self.clear_prop,
        )
    }
}
