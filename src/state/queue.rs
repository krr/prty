use super::rule::Rule;
use data::Pack;
use data::{Atom, NestedDefinition};
use util::bitvec::BitVec;
use util::heap::BinaryHeapCmp;
use util::compare::Natural;
use util::heap_order::BHeapOrder;
use util::heap_order::typenum::U7;
use util::reverse_index::NoRI;
//use std::collections::BinaryHeap;

type BinaryHeap<T> = BinaryHeapCmp<T, Natural<T>, NoRI<T>, BHeapOrder<U7>>;

#[derive(Default, Debug)]
pub struct Queue {
    in_queue: BitVec,
    has_unfounded: BitVec,
    prop_by_level: BinaryHeap<u64>,
}
impl From<Pack> for Rule {
    fn from(Pack(idx, atom): Pack) -> Self {
        if atom == Atom::invalid() {
            Rule::Unfounded(idx)
        } else {
            Rule::Prop(idx, atom)
        }
    }
}
impl From<Rule> for Pack {
    fn from(r: Rule) -> Self {
        match r {
            Rule::Prop(idx, atom) => Pack(idx, atom),
            Rule::Unfounded(idx) => Pack(idx, Atom::invalid()),
        }
    }
}
impl From<Rule> for u64 {
    fn from(r: Rule) -> Self {
        u64::max_value() - u64::from(Pack::from(r))
    }
}
impl From<u64> for Rule {
    fn from(u: u64) -> Self {
        Rule::from(Pack::from(u64::max_value() - u))
    }
}
impl Queue {
    pub fn build(def: &NestedDefinition) -> Queue {
        Queue {
            in_queue: BitVec::with_capacity(def.nb_atoms()),
            has_unfounded: BitVec::with_capacity(def.nb_definitions()),
            prop_by_level: BinaryHeap::with_capacity(def.nb_atoms() + def.nb_definitions()),
            ..Default::default()
        }
    }
    pub fn set(&mut self, r: Rule, value: bool) -> bool {
        match r {
           Rule::Prop(_, atom) => self.in_queue.set(atom.idx(), value),
           Rule::Unfounded(idx) => self.has_unfounded.set(idx.i(), value),
       }
    }
    pub fn push(&mut self, r: Rule) {
        if !self.set(r, true) {
            self.prop_by_level.push(u64::from(r));
        }
    }
    pub fn pop(&mut self) -> Option<Rule> {
        let prop_unfounded = Rule::from(self.prop_by_level.pop()?);
        self.set(prop_unfounded, false);
        Some(prop_unfounded)
    }
    pub fn peek(&self) -> Option<Rule> {
        self.prop_by_level.peek().cloned().map(Rule::from)
    }
    pub fn is_empty(&self) -> bool {
        self.prop_by_level.is_empty()
    }
}
#[cfg(test)]
mod test {
    use super::*;
    use data::DefIdx;
    #[test]
    fn bijective_rule() {
        let r1 = Rule::Prop(DefIdx::new(12), Atom::new(123));
        let r2 = Rule::Unfounded(DefIdx::new(13));
        assert_eq!(r1, Rule::from(Pack::from(r1)));
        assert_eq!(r2, Rule::from(Pack::from(r2)));
        assert_eq!(r1, Rule::from(u64::from(r1)));
        assert_eq!(r2, Rule::from(u64::from(r2)));
    }
}
