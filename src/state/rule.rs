use data::{Atom, DefIdx};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Rule {
    Prop(DefIdx, Atom),
    Unfounded(DefIdx),
}

impl Rule {
    pub fn lex_order(&self) -> (usize, usize) {
        use self::Rule::*;
        match *self {
            Prop(def, atom) => (usize::max_value() - def.i(), atom.idx()),
            Unfounded(def) => (usize::max_value() - def.i(), usize::max_value()),
        }
    }
    pub fn location(&self) -> DefIdx {
        match *self {
            Rule::Prop(def, _) => def,
            Rule::Unfounded(def) => def,
        }
    }
    pub fn matches(&self, r: Rule) -> bool {
        use self::Rule::*;
        match (*self, r) {
            (Prop(d, _) , Prop(d2, _)) => d == d2,
            (Unfounded(d), Unfounded(d2)) => d == d2,
            _ => false,
        }
    }
}

impl PartialOrd for Rule {
    fn partial_cmp(&self, other: &Rule) -> Option<::std::cmp::Ordering> {
        let ord = self.lex_order().cmp(&other.lex_order());
        Some(ord)
    }
}
impl Ord for Rule {
    fn cmp(&self, other: &Rule) -> ::std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}
