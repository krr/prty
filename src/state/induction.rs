use super::rule::Rule;
use super::State;
use bitgraph::Graph;
use data::interpretation::{Eval, Witness};
use data::{DefIdx, NestedDefinition, NestingRelation};

impl State {
    pub fn reset(&mut self, def: &NestedDefinition) {
        let def_idx = DefIdx::open();
        self.graph.clear();
        def.atoms()
            .filter(|a| def.sub_of(*a, def_idx))
            .for_each(|a| {
                self.interpretation.set(&a, false);
                self.set_justified(a, false);
                self.queue.push(Rule::Prop(def.def(&a), a));
            });
    }
    pub fn induct_generic<F>(&mut self, def: &NestedDefinition, f: F)
    where
        F: Fn(&mut State, &NestedDefinition),
    {
        //cd for i in 1..700 {
        while !self.done() {
            f(self, def);
        }
    }
    pub fn unsupport(&mut self, def: &NestedDefinition) {
        while let Some(atom) = self.other_queue.pop() {
            self.graph.clear_all_to(&atom);
            let was_justified = self.set_justified(atom, false);
            let was_true = self.interpretation.set(&atom, false);
            self.queue.push(Rule::Prop(def.def(&atom), atom));
            if was_justified {
                for depending in self.graph.iter_from_map_nobox(atom) {
                    if self.is_justified.get(depending.idx()) || was_true {
                        self.other_queue.push(depending);
                    }
                }
            }
        }
    }
    pub fn induct_just_step(&mut self, def: &NestedDefinition) {
        let rule = if let Some(rule) = self.queue.peek() {
            rule
        } else {
            return;
        };
        use self::Rule::*;
        match rule {
            Prop(idx, _) => {
                while let Some(Prop(d, a)) = self.queue.peek() {
                    if idx != d {
                        break;
                    }
                    let _rule = self.queue.pop();
                    let Witness(is_true, w) = self.interpretation.eval(def.rule(&a));
                    self.graph.set_all_to(&a, w);
                    if is_true {
                        self.interpretation.set(&a, true);
                        self.set_justified(a, true);
                        self.other_queue.extend(self.graph.iter_from_map_nobox(a));
                    } else {
                        self.queue.push(Rule::Unfounded(def.def(&a)));
                    }
                }
                self.unsupport(def);
            }
            Unfounded(def_idx) => {
                self.queue.pop();
                def.atoms_of.set_all(def_idx, &mut self.is_justified);
            }
        }
    }
}
