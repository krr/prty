use data::*;
use state::DefInter;
use state::interpretation::Inter;

pub fn e5() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::Or(vec![fact!(-1), fact!(+2)]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::And(vec![fact!(-0)]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::And(vec![fact!(+0)]),
                    def: DefIdx(3),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(1)],
            signs: signs![+, -, +, -],
            node_namer: (),
        },
        Inter::vec(vec![false, false, false]),
    )
}
pub fn e6() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::And(vec![]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![fact!(-0)]),
                    def: DefIdx(3),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(1)],
            signs: signs![+, +, -, +],
            node_namer: (),
        },
        Inter::vec(vec![false, false]),
    )
}
pub fn e7() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::And(vec![]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![fact!(-2), fact!(-0)]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::Or(vec![fact!(+0)]),
                    def: DefIdx(4),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(2), DefIdx(3)],
            signs: signs![-, +, -, +, -],
            node_namer: (),
        },
        Inter::vec(vec![false, false, false]),
    )
}
pub fn e8() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::Or(vec![fact!(+0), fact!(-1), fact!(-3)]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::And(vec![fact!(-2)]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::And(vec![fact!(+0), fact!(-1)]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![fact!(+3)]),
                    def: DefIdx(4),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(2), DefIdx(1)],
            signs: signs![+, +, -, +, -],
            node_namer: (),
        },
        Inter::vec(vec![false, false, false, false]),
    )
}

pub fn e3() -> DefInter<()> {
    let r1 = Rule {
        body: Body::Or(vec![fact!(+1)]),
        def: DefIdx(1),
    };
    let r2 = Rule {
        body: Body::And(vec![]),
        def: DefIdx(1),
    };
    let parent = vec![DefIdx(0), DefIdx(0)];
    let inter = Inter::new(2);
    let def = NestedDefinition {
        rules: vec![r1, r2],
        parent,
        signs: vec![Sign::Neg, Sign::Neg],
        node_namer: (),
    };
    DefInter(def, inter)
}
pub fn e10() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::Or(vec![fact!(-1), fact!(-3)]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![fact!(+2), fact!(+3), fact!(-0)]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::Or(vec![fact!(+1), fact!(+2), fact!(+3)]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![fact!(+1), fact!(+3), fact!(-0)]),
                    def: DefIdx(3),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(2)],
            signs: signs![-, +, -, +],
            node_namer: (),
        },
        Inter::vec(vec![false, false, false, false]),
    )
}

pub fn e11() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::Or(vec![fact!(-3)]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::And(vec![fact!(+0), fact!(+1), fact!(-2)]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![fact!(+3), fact!(-1)]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::Or(vec![]),
                    def: DefIdx(3),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(2)],
            signs: signs![-, -, -, +],
            node_namer: (),
        },
        Inter::vec(vec![false, false, false, false]),
    )
}
pub fn e12() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::Or(vec![fact!(+4), fact!(+3)]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::Or(vec![fact!(-0), fact!(-2)]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::Or(vec![fact!(+0)]),
                    def: DefIdx(4),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(2), DefIdx(1)],
            signs: signs![-, +, -, +, +],
            node_namer: (),
        },
        Inter::vec(vec![false, false, false, false, false]),
    )
}
pub fn e9() -> DefInter<()> {
    DefInter(
        NestedDefinition {
            rules: vec![
                Rule {
                    body: Body::Open,
                    def: DefIdx(0),
                },
                Rule {
                    body: Body::Open,
                    def: DefIdx(0),
                },
                Rule {
                    body: Body::Open,
                    def: DefIdx(0),
                },
                Rule {
                    body: Body::Open,
                    def: DefIdx(0),
                },
                Rule {
                    body: Body::Open,
                    def: DefIdx(0),
                },
                Rule {
                    body: Body::Open,
                    def: DefIdx(0),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+0),
                        fact!(+4),
                        fact!(+7),
                        fact!(+9),
                        fact!(+24),
                        fact!(+26),
                        fact!(+28),
                        fact!(+29),
                        fact!(-14),
                        fact!(-15),
                        fact!(-20),
                        fact!(-21),
                        fact!(-22),
                    ]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+3),
                        fact!(+8),
                        fact!(+10),
                        fact!(+11),
                        fact!(+25),
                        fact!(+26),
                        fact!(+27),
                        fact!(-12),
                        fact!(-16),
                        fact!(-17),
                        fact!(-18),
                        fact!(-19),
                        fact!(-20),
                        fact!(-21),
                        fact!(-23),
                    ]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+0),
                        fact!(+1),
                        fact!(+2),
                        fact!(+4),
                        fact!(+8),
                        fact!(+28),
                        fact!(+29),
                        fact!(-13),
                        fact!(-14),
                        fact!(-15),
                        fact!(-16),
                        fact!(-17),
                        fact!(-18),
                        fact!(-21),
                        fact!(-23),
                    ]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+0),
                        fact!(+1),
                        fact!(+3),
                        fact!(+4),
                        fact!(+5),
                        fact!(+7),
                        fact!(+9),
                        fact!(+10),
                        fact!(+11),
                        fact!(+26),
                        fact!(+27),
                        fact!(+28),
                        fact!(+29),
                        fact!(-12),
                        fact!(-15),
                        fact!(-18),
                        fact!(-20),
                        fact!(-21),
                        fact!(-23),
                    ]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+0),
                        fact!(+1),
                        fact!(+2),
                        fact!(+4),
                        fact!(+6),
                        fact!(+9),
                        fact!(+11),
                        fact!(+24),
                        fact!(+25),
                        fact!(+26),
                        fact!(+27),
                        fact!(+28),
                        fact!(+29),
                        fact!(-12),
                        fact!(-13),
                        fact!(-15),
                        fact!(-17),
                        fact!(-18),
                        fact!(-20),
                    ]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+0),
                        fact!(+2),
                        fact!(+3),
                        fact!(+5),
                        fact!(+6),
                        fact!(+11),
                        fact!(+24),
                        fact!(+27),
                        fact!(+28),
                        fact!(-12),
                        fact!(-13),
                        fact!(-14),
                        fact!(-15),
                        fact!(-20),
                        fact!(-21),
                        fact!(-22),
                    ]),
                    def: DefIdx(1),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+13),
                        fact!(+14),
                        fact!(+15),
                        fact!(-1),
                        fact!(-3),
                        fact!(-5),
                        fact!(-6),
                        fact!(-11),
                    ]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+14),
                        fact!(+15),
                        fact!(+17),
                        fact!(-2),
                        fact!(-3),
                        fact!(-6),
                        fact!(-9),
                        fact!(-10),
                        fact!(-11),
                    ]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+13),
                        fact!(+15),
                        fact!(+16),
                        fact!(-0),
                        fact!(-1),
                        fact!(-3),
                        fact!(-5),
                        fact!(-6),
                        fact!(-9),
                    ]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+12),
                        fact!(+15),
                        fact!(+17),
                        fact!(-4),
                        fact!(-5),
                        fact!(-6),
                        fact!(-7),
                        fact!(-9),
                    ]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+13),
                        fact!(+16),
                        fact!(+17),
                        fact!(-0),
                        fact!(-3),
                        fact!(-4),
                        fact!(-5),
                        fact!(-8),
                        fact!(-10),
                        fact!(-11),
                    ]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+16),
                        fact!(-1),
                        fact!(-2),
                        fact!(-3),
                        fact!(-4),
                        fact!(-5),
                        fact!(-9),
                        fact!(-11),
                    ]),
                    def: DefIdx(2),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+22),
                        fact!(+23),
                        fact!(-0),
                        fact!(-1),
                        fact!(-4),
                        fact!(-5),
                        fact!(-6),
                        fact!(-8),
                        fact!(-9),
                        fact!(-11),
                        fact!(-24),
                        fact!(-27),
                    ]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+19),
                        fact!(+20),
                        fact!(+21),
                        fact!(+23),
                        fact!(-2),
                        fact!(-5),
                        fact!(-6),
                        fact!(-7),
                        fact!(-8),
                        fact!(-9),
                        fact!(-10),
                        fact!(-11),
                        fact!(-27),
                    ]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+20),
                        fact!(+21),
                        fact!(+22),
                        fact!(+23),
                        fact!(-0),
                        fact!(-1),
                        fact!(-2),
                        fact!(-4),
                        fact!(-5),
                        fact!(-8),
                        fact!(-10),
                        fact!(-24),
                        fact!(-25),
                        fact!(-28),
                        fact!(-29),
                    ]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+22),
                        fact!(+23),
                        fact!(-0),
                        fact!(-1),
                        fact!(-3),
                        fact!(-4),
                        fact!(-5),
                        fact!(-9),
                        fact!(-24),
                        fact!(-26),
                        fact!(-29),
                    ]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+18),
                        fact!(+20),
                        fact!(+21),
                        fact!(+22),
                        fact!(+23),
                        fact!(-1),
                        fact!(-2),
                        fact!(-3),
                        fact!(-4),
                        fact!(-9),
                        fact!(-11),
                        fact!(-24),
                        fact!(-25),
                        fact!(-26),
                    ]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+18),
                        fact!(+21),
                        fact!(+22),
                        fact!(+23),
                        fact!(-0),
                        fact!(-2),
                        fact!(-3),
                        fact!(-4),
                        fact!(-6),
                        fact!(-7),
                        fact!(-8),
                        fact!(-9),
                        fact!(-10),
                        fact!(-24),
                        fact!(-25),
                        fact!(-28),
                    ]),
                    def: DefIdx(3),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+0),
                        fact!(+2),
                        fact!(+3),
                        fact!(+4),
                        fact!(+5),
                        fact!(+6),
                        fact!(+7),
                        fact!(+11),
                        fact!(+27),
                        fact!(+28),
                        fact!(+29),
                        fact!(-19),
                        fact!(-20),
                        fact!(-22),
                    ]),
                    def: DefIdx(4),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+0),
                        fact!(+4),
                        fact!(+5),
                        fact!(+6),
                        fact!(+8),
                        fact!(+9),
                        fact!(+26),
                        fact!(+27),
                        fact!(+28),
                        fact!(+29),
                        fact!(-18),
                        fact!(-20),
                        fact!(-22),
                    ]),
                    def: DefIdx(4),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+5),
                        fact!(+6),
                        fact!(+7),
                        fact!(+11),
                        fact!(+24),
                        fact!(+26),
                        fact!(+29),
                    ]),
                    def: DefIdx(4),
                },
                Rule {
                    body: Body::Or(vec![
                        fact!(+1),
                        fact!(+2),
                        fact!(+3),
                        fact!(+4),
                        fact!(+5),
                        fact!(+6),
                        fact!(+7),
                        fact!(+8),
                        fact!(+26),
                        fact!(+28),
                        fact!(-19),
                        fact!(-21),
                        fact!(-23),
                    ]),
                    def: DefIdx(4),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+1),
                        fact!(+4),
                        fact!(+6),
                        fact!(+7),
                        fact!(+8),
                        fact!(+9),
                        fact!(+11),
                        fact!(+25),
                        fact!(+29),
                        fact!(-18),
                        fact!(-20),
                        fact!(-21),
                        fact!(-23),
                    ]),
                    def: DefIdx(4),
                },
                Rule {
                    body: Body::And(vec![
                        fact!(+1),
                        fact!(+5),
                        fact!(+7),
                        fact!(+9),
                        fact!(+25),
                        fact!(+27),
                        fact!(+28),
                        fact!(-19),
                        fact!(-21),
                        fact!(-22),
                    ]),
                    def: DefIdx(4),
                },
            ],
            parent: vec![DefIdx(0), DefIdx(0), DefIdx(1), DefIdx(1), DefIdx(3), DefIdx(4)],
            signs: signs![+, +, -, -, +, +],
            node_namer: (),
        },
        Inter::vec(vec![
            true, false, true, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false,
        ]),
    )
}
