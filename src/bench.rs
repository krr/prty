use data::DefInter;
use data::nested_definition::*;
use fmt_modifier::*;
use quickcheck::{Arbitrary, StdGen};
use rand::{Rng, SeedableRng, StdRng};
use state::watches::*;
use test::Bencher;

#[bench]
fn bench_add_two(b: &mut Bencher) {
    let mut rng = StdRng::from_seed(&[0]);
    b.iter(|| {
        let mut gen = StdGen::new(StdRng::from_seed(&[rng.gen()]), 800);
        let def: DefInter = DefInter::arbitrary(&mut gen);
        println!("{:?}", PrintRefAs(&def.0, BasicStat));
        let just = def.induct::<WakeGraph>();
        just.verify_loops(&def.0).is_ok()
    });
}
