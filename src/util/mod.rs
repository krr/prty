pub extern crate compare;

pub mod sign;
pub mod sorted_queue;
pub mod tribool;
pub mod vec;
pub mod permutation;
pub mod bitvec;
pub mod remapper;
pub mod heap;
pub mod heap_order;
pub mod reverse_index;
pub mod hole;
