use std::collections::BTreeSet;

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct SortedQueue<T>(BTreeSet<T>);

use std::ops::{Deref, DerefMut};
impl<T> Deref for SortedQueue<T> {
    type Target = BTreeSet<T>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<T> DerefMut for SortedQueue<T> {
    fn deref_mut(&mut self) -> &mut <Self as Deref>::Target {
        &mut self.0
    }
}

impl<T: Ord + Clone> SortedQueue<T> {
    pub fn push(&mut self, t: T) {
        self.0.insert(t);
    }
    pub fn peek(&self) -> Option<&T> {
        self.0.iter().next()
    }
    pub fn pop(&mut self) -> Option<T> {
        if let Some(e) = self.0.iter().next().cloned() {
            self.0.take(&e)
        } else {
            None
        }
    }
}
impl<T: Ord> Default for SortedQueue<T> {
    fn default() -> Self {
        SortedQueue(BTreeSet::default())
    }
}
