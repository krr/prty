use std::collections::HashMap;
use std::hash::Hash;

#[derive(Debug, Default, Clone, Eq, PartialEq)]
pub struct Remapper<T: Hash + Eq> {
    map: HashMap<T, usize>,
    inverse: Vec<T>,
}

impl<T: Hash + Clone + Eq> Remapper<T> {
    pub fn insert(&mut self, t: T) -> usize {
        if let Some(idx) = self.map.get(&t) {
            return *idx;
        }
        let idx = self.inverse.len();
        self.map.insert(t.clone(), idx);
        self.inverse.push(t);
        idx
    }
    pub fn get(&self, t: &T) -> Option<usize> {
        self.map.get(t).map(Clone::clone)
    }
    pub fn invert(&self, idx: usize) -> Option<&T> {
        self.inverse.get(idx)
    }
    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.inverse.iter()
    }
}
