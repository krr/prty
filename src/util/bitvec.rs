#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct BitVec(Vec<u64>);

impl BitVec {
    pub fn decompose(idx: usize) -> (usize, usize) {
        let limb = idx / 64;
        let pos = idx % 64;
        (limb, pos)
    }
    pub fn with_capacity(len: usize) -> BitVec {
        let limbs = (len - 1) / 64 + 1;
        BitVec(::util::vec::vec_default(limbs))
    }
    pub fn ensure(&mut self, idx: usize) {
        let (limb, _) = Self::decompose(idx);
        super::vec::vec_extend_until_valid(&mut self.0, limb)
    }
    pub fn set(&mut self, idx: usize, value: bool) -> bool {
        let (limb, pos) = Self::decompose(idx);
        let ret = (self.0[limb] & (1 << pos)) != 0;
        self.0[limb] &= !(1 << pos);
        self.0[limb] |= (if value { 1 } else { 0 }) << pos;
        ret
    }
    pub fn set_all(&mut self, from: usize, to: usize) {
        let (limb_f, pos_f) = Self::decompose(from);
        let (limb_t, pos_t) = Self::decompose(to - 1);
        for idx in (limb_f + 1)..(limb_t) {
            self.0[idx] = !0;
        }
        let mask_f = (!0) << pos_f;
        let mask_t = (!0) >> (63-pos_t);
        if limb_f == limb_t {
            if from != to {
                self.0[limb_f] |= mask_f & mask_t;
            }
        } else {
            self.0[limb_f] |= mask_f;
            self.0[limb_t] |= mask_t;
        }
    }
    pub fn get(&self, idx: usize) -> bool {
        let (limb, pos) = Self::decompose(idx);
        (self.0[limb] & 1 << pos) != 0
    }

    pub fn iter(&self) -> BitIter {
        BitIter {
            vec: self,
            limb: 0,
            word: 0,
        }
    }
}

pub struct BitIter<'a> {
    vec: &'a BitVec,
    limb: usize,
    word: u64,
}

impl<'a> Iterator for BitIter<'a> {
    type Item = usize;
    fn next(&mut self) -> Option<usize> {
        while self.word == 0 {
            self.word = *self.vec.0.get(self.limb)?;
            self.limb += 1;
        }
        let pos = self.word.trailing_zeros() as usize;
        self.word &= !(1 << pos);
        Some(self.limb * 64 + pos)
    }
    fn count(self) -> usize {
        let mut total = self.word.count_ones() as usize;
        let mut limb = self.limb;
        while let Some(word) = self.vec.0.get(limb) {
            total += word.count_ones() as usize;
            limb += 1;
        }
        total
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn set() {
        extern crate test;
        use test::black_box;
        for _ in 0..5000000 {
            let mut vec = BitVec::with_capacity(5000);
            vec.set(1, true);
            vec.set(2, true);
            vec.set_all(60, 70);
            vec.set_all(128, 192);
            let f = black_box(200);
            let t = black_box(4500);
            vec.set_all(f, t);

            vec.set(62, false);
            vec.set(63, false);
            let c = (0..500).into_iter().filter(|x| vec.get(*x)).count();
            assert_eq!(c, 2 + 10 + 64 + 300 - 2);
        }
    }
}
