use std::collections::HashMap;
use std::hash::Hash;
use std::marker::PhantomData;

pub trait ReverseIndex<T> {
    fn new(&mut self, &T, usize);
    fn update(&mut self, &T, usize);
    fn remove(&mut self, _: &T, _: usize);
}
pub trait CheckReverseIndex<T> : ReverseIndex<T> {
        fn check(&self, &T) -> bool;
}
pub trait FindReverseIndex<T>: CheckReverseIndex<T> {
    fn find(&self, &T) -> Option<usize>;
}
#[derive(Debug)]
pub struct NoRI<T> {
    t: PhantomData<T>,
}
impl<T> Default for NoRI<T> {
    fn default() -> Self {
        Self{t: PhantomData}
    }
}
impl<T> ReverseIndex<T> for NoRI<T> {
    fn new(&mut self, _: &T, _: usize) {}
    fn update(&mut self, _: &T, _: usize) {}
    fn remove(&mut self, _: &T, _: usize) {}
}

#[derive(Default)]
pub struct HashRI<T: Eq + Clone + Hash> {
    map: HashMap<T, usize>,
}
impl<T: Eq + Clone + Hash> HashRI<T> {
    pub fn len(&self) -> usize {
        self.map.len()
    }
}
impl<T: Eq + Clone + Hash> ReverseIndex<T> for HashRI<T> {
    fn new(&mut self, elem: &T, idx: usize) {
        let removed = self.map.insert(elem.clone(), idx);
        assert!(removed.is_none(), "Element already in index");
    }
    fn update(&mut self, elem: &T, idx: usize) {
        let val = self.map
            .get_mut(elem)
            .expect("Didn't find element to update");
        *val = idx;
    }
    fn remove(&mut self, elem: &T, idx: usize) {
        let idx_orig = self.map.remove(elem);
        debug_assert_eq!(idx_orig, Some(idx));
    }
}
impl<T: Eq + Clone + Hash> CheckReverseIndex<T> for HashRI<T> {
    fn check(&self, elem: &T) -> bool {
        self.map.contains_key(elem)
    }
}
impl<T: Eq + Clone + Hash> FindReverseIndex<T> for HashRI<T> {
    fn find(&self, elem: &T) -> Option<usize> {
        self.map.get(elem).map(|idx| *idx)
    }
}
