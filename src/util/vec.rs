pub fn vec_extend_by<T: Default>(vec: &mut Vec<T>, len: usize) {
    vec.reserve(len);
    for _ in 0..len {
        vec.push(Default::default());
    }
}
pub fn vec_extend_until<T: Default>(vec: &mut Vec<T>, len: usize) {
    if len > vec.len() {
        let with = len - vec.len();
        vec_extend_by(vec, with)
    }
}
pub fn vec_extend_until_valid<T: Default>(vec: &mut Vec<T>, idx: usize) {
    vec_extend_until(vec, idx + 1)
}

pub fn set_at<T: Default>(vec: &mut Vec<T>, idx: usize, mut t: T) -> T {
    vec_extend_until_valid(vec, idx);
    ::std::mem::swap(&mut vec[idx], &mut t);
    t
}

pub fn vec_default<T: Default>(len: usize) -> Vec<T> {
    let mut vec = Vec::with_capacity(len);
    vec_extend_until(&mut vec, len);
    vec
}
