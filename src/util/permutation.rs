pub struct Permutation(Vec<usize>);
impl From<Vec<usize>> for Permutation {
    fn from(t: Vec<usize>) -> Self {
        Permutation(t)
    }
}
impl Default for Permutation {
    fn default() -> Permutation {
        Permutation(vec![])
    }
}
impl Permutation {
    pub fn permute<T>(&self, a: &T) -> T where usize: for<'a> From<&'a T>, T: From<usize> {
        let idx = usize::from(a);
        self.0.get(idx).cloned().unwrap_or(idx).into()
    }
    pub fn invert(&self) -> Permutation {
        let mut vec = vec![0; self.0.len()];
        for (from, to) in self.0.iter().enumerate() {
            vec[*to] = from;
        }
        Permutation(vec)
    }
}
