pub extern crate typenum;
use self::typenum::Unsigned;

pub trait HeapOrder {
    fn parent(&self, idx: usize) -> usize;
    fn children(&self, idx: usize) -> (usize, usize);
    fn left_child(&self, idx: usize) -> usize {
        self.children(idx).0
    }
    fn right_child(&self, idx: usize) -> usize {
        self.children(idx).1
    }
    fn next_child(&self, idx: usize) -> usize {
        self.right_child(self.parent(idx))
    }
}
#[derive(Default, Debug, Ord, PartialEq, Eq, PartialOrd, Clone, Copy)]
pub struct BinaryHeapOrder;
impl HeapOrder for BinaryHeapOrder {
    fn parent(&self, child: usize) -> usize {
        (child - 1) / 2
    }
    fn children(&self, parent: usize) -> (usize, usize) {
        let left = parent * 2 + 1;
        (left, left + 1)
    }
    fn next_child(&self, idx: usize) -> usize {
        idx + 1
    }
}

#[derive(Default, Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub struct BHeapOrder<M: Unsigned>(::std::marker::PhantomData<M>);
impl<M: Unsigned> BHeapOrder<M> {
    #[inline]
    fn bit() -> usize {
        1 << (M::to_usize() - 1)
    }
    #[inline]
    fn mask() -> usize {
        (1 << M::to_usize()) - 1
    }
    #[inline]
    fn little_mask() -> usize {
        Self::bit() - 1
    }
}
impl<M: Unsigned> HeapOrder for BHeapOrder<M> {
    fn parent(&self, idx: usize) -> usize {
        if idx == 0 {
            0
        } else if idx & Self::mask() == 0 {
            let idx = (idx - 2 * Self::bit()) >> (M::to_usize() + 1);
            let x = idx & !Self::little_mask();
            idx + x + Self::bit()
        } else {
            let x = idx & !Self::mask();
            let y = idx & Self::mask();
            x | (y >> 1)
        }
    }
    fn children(&self, mut idx: usize) -> (usize, usize) {
        let y = idx & Self::mask();
        let mut z = 1;
        idx = idx + y;
        if y == 0 {
            idx = idx + 1;
            z = 0;
        }
        if y & Self::bit() != 0 {
            idx = idx - 2 * Self::bit() + 1;
            idx = idx << M::to_usize();
            z = 2*Self::bit();
        }
        (idx, idx + z)
    }
    fn next_child(&self, idx: usize) -> usize {
        if idx & Self::mask() == 0 {
            idx + 2 * Self::bit()
        } else if idx & Self::mask() == 1 {
            idx
        } else {
            idx + 1
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn order() {
        use super::typenum::U3;
        type ThreeOrder = BHeapOrder<U3>;
        let order = ThreeOrder::default();
        assert_eq!(order.children(0), (1, 1));
        assert_eq!(order.children(1), (2, 3));
        assert_eq!(order.children(3), (6, 7));
        assert_eq!(order.children(6), (40, 48));
        assert_eq!(order.children(68), (520, 528));
        assert_eq!(order.parent(0), 0);
        assert_eq!(order.parent(1), 0);
        assert_eq!(order.parent(3), 1);
        assert_eq!(order.parent(7), 3);
        assert_eq!(order.parent(5), 2);
        assert_eq!(order.parent(64), 7);
        assert_eq!(order.parent(40), 6);
        assert_eq!(order.parent(8), 4);
        assert_eq!(order.parent(520), 68);
    }

    #[test]
    fn bijection() {
        fn bijection_for<M: Unsigned + Default>() {
            let order = BHeapOrder::<M>::default();
            for i in 0..1000 {
                let (c1, c2) = order.children(i);
                assert_eq!(order.parent(c1), i);
                assert_eq!(order.parent(c2), i);
            }
        }
        bijection_for::<super::typenum::U3>();
        bijection_for::<super::typenum::U8>();
        bijection_for::<super::typenum::U9>();
    }
}
