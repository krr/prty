use super::compare::{Compare, Natural};
use super::heap_order::{BinaryHeapOrder, HeapOrder};
use super::reverse_index::{CheckReverseIndex, FindReverseIndex, NoRI, ReverseIndex};
use std::iter;

/// Max heap
#[derive(Debug)]
pub struct BinaryHeapCmp<T, Cmp = Natural<T>, RI = NoRI<T>, Order = BinaryHeapOrder>
where
    Order: HeapOrder,
    Cmp: Compare<T, T>,
    RI: ReverseIndex<T>,
{
    pub data: Vec<T>,
    order: Order,
    cmp: Cmp,
    reverse_index: RI,
}

impl<T, Cmp, RI, Order> Default for BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: Default + HeapOrder,
    Cmp: Default + Compare<T, T>,
    RI: Default + ReverseIndex<T>,
{
    fn default() -> Self {
        BinaryHeapCmp {
            data: Default::default(),
            order: Default::default(),
            cmp: Default::default(),
            reverse_index: Default::default(),
        }
    }
}
impl<T, Cmp, RI, Order> BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: Default + HeapOrder,
    Cmp: Default + Compare<T, T>,
    RI: Default + ReverseIndex<T>,
{
    pub fn with_capacity(capacity: usize) -> Self {
        BinaryHeapCmp {
            data: Vec::with_capacity(capacity),
            order: Default::default(),
            cmp: Default::default(),
            reverse_index: Default::default(),
        }
    }
}

impl<T, Cmp, RI, Order> iter::Extend<T> for BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: HeapOrder,
    Cmp: Compare<T, T>,
    RI: ReverseIndex<T>,
{
    #[inline]
    fn extend<I: IntoIterator<Item = T>>(&mut self, iter: I) {
        let iter = iter.into_iter();
        self.data.reserve(iter.size_hint().0);
        for item in iter {
            self.push(item);
        }
    }
}
impl<T, Order: Default, Cmp: Default, RI: Default> iter::FromIterator<T>
    for BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: HeapOrder,
    Cmp: Compare<T, T>,
    RI: ReverseIndex<T>,
{
    fn from_iter<IT>(iter: IT) -> Self
    where
        IT: IntoIterator<Item = T>,
    {
        let mut out = Self::default();
        out.extend(iter);
        out
    }
}

impl<T, Cmp, RI, Order> BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: HeapOrder + Default,
    Cmp: Compare<T, T>,
    RI: ReverseIndex<T> + Default,
{
    pub fn new_cmp(comparator: Cmp) -> Self {
        BinaryHeapCmp {
            data: Vec::new(),
            cmp: comparator,
            reverse_index: RI::default(),
            order: Order::default(),
        }
    }
}
impl<T, Cmp, RI, Order> BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: HeapOrder + Default,
    Cmp: Compare<T, T> + Default,
    RI: ReverseIndex<T>,
{
    pub fn new_ri(rev_index: RI) -> Self {
        BinaryHeapCmp {
            data: Vec::new(),
            cmp: Cmp::default(),
            reverse_index: rev_index,
            order: Order::default(),
        }
    }
}
impl<T, Cmp, RI, Order> BinaryHeapCmp<T, Cmp, RI, Order>
where
    Order: HeapOrder,
    Cmp: Compare<T, T>,
    RI: ReverseIndex<T>,
{
    pub fn iter(&self) -> ::std::slice::Iter<T> {
        self.data.iter()
    }
    pub fn new(cmp: Cmp, reverse_index: RI, order: Order) -> Self {
        BinaryHeapCmp {
            data: vec![],
            cmp,
            reverse_index,
            order,
        }
    }
    pub fn verify_integrity(&self) -> bool {
        let len = self.data.len();
        for index in 0..len {
            let (lc, rc) = self.order.children(index);
            if lc < len && self.cmp.compares_gt(&self.data[lc], &self.data[index]) {
                return false;
            } else if rc < len && self.cmp.compares_gt(&self.data[rc], &self.data[index]) {
                return false;
            }
        }
        return true;
    }
    pub fn get_reverse_index(&self) -> &RI {
        &self.reverse_index
    }
    pub fn peek(&self) -> Option<&T> {
        self.data.get(0)
    }
    pub unsafe fn peek_mut(&mut self) -> Option<&mut T> {
        self.data.get_mut(0)
    }
    pub fn peek_idx(&self, idx: usize) -> Option<&T> {
        self.data.get(idx)
    }
    pub unsafe fn peek_idx_mut(&mut self, idx: usize) -> Option<&mut T> {
        self.data.get_mut(idx)
    }
    pub fn push(&mut self, elem: T) {
        let i = self.len();
        self.reverse_index.new(&elem, i);
        self.data.push(elem);
        self.swim(i);
    }
    pub fn pop(&mut self) -> Option<T> {
        if self.len() == 0 {
            None
        } else {
            Some(self.take(0))
        }
    }
    pub unsafe fn rebuild(&mut self) {
        let len = self.data.len();
        for i in (0..len).rev() {
            self.sink(i);
        }
    }
    pub fn len(&self) -> usize {
        self.data.len()
    }
    pub fn is_empty(&self) -> bool {
        self.data.len() == 0
    }
    pub fn take(&mut self, idx: usize) -> T {
        let out = self.data.swap_remove(idx);
        self.reverse_index.remove(&out, idx);
        self.sink_to_bottom(idx);
        out
    }
    fn sink_to_bottom(&mut self, mut pos: usize) {
        use super::hole::Hole;
        let start = pos;
        let end = self.len();
        if start >= end {
            return;
        }
        unsafe {
            let mut hole = Hole::new(&mut self.data, pos);
            let (mut lc, mut rc) = self.order.children(pos);
            debug_assert!(lc <= rc);
            while rc < end {
                if self.cmp.compares_le(hole.get(lc), hole.get(rc)) {
                    pos = rc;
                } else {
                    pos = lc;
                }
                let old = hole.move_to(pos);
                self.reverse_index.update(hole.get(old), old);
                let (l, r) = self.order.children(pos);
                lc = l;
                rc = r;
                debug_assert!(lc <= rc);
            }
            if lc < end {
                let old = hole.move_to(lc);
                self.reverse_index.update(hole.get(old), old);
            }
            pos = hole.pos();
        }
        self.swim_to(start, pos);
    }
    fn sink(&mut self, mut index: usize) {
        let len = self.len();
        while index < len {
            let mut swap_with = index;
            let (lc, rc) = self.order.children(index);
            if lc < len && self.cmp.compares_gt(&self.data[lc], &self.data[swap_with]) {
                swap_with = lc;
            }
            if rc < len && self.cmp.compares_gt(&self.data[rc], &self.data[swap_with]) {
                swap_with = rc;
            }
            if index == swap_with {
                break;
            }
            self.data.swap(index, swap_with);
            self.reverse_index.update(&self.data[index], index);
            index = swap_with;
        }
        if index != len {
            self.reverse_index.update(&self.data[index], index)
        }
    }
    fn swim(&mut self, index: usize) {
        self.swim_to(0, index);
    }
    fn swim_to(&mut self, start: usize, index: usize) {
        use super::hole::Hole;
        unsafe{
            let mut hole = Hole::new(&mut self.data, index);
            while hole.pos() > start {
                let p = self.order.parent(hole.pos());
                if self.cmp.compares_ge(hole.get(p), hole.element()) {
                    break;
                }
                let old = hole.move_to(p);
                self.reverse_index.update(hole.get(old), old);
            }
            self.reverse_index.update(hole.element(), hole.pos());
            drop(hole);
        }
    }
}

impl<'a, T, Order: HeapOrder, Cmp: Compare<T, T>, RI: FindReverseIndex<T>>
    BinaryHeapCmp<T, Cmp, RI, Order>
{
    pub fn remove_reverse_index(&mut self, elem: &T) -> Option<T> {
        self.reverse_index.find(elem).map(|idx| self.take(idx))
    }
    pub fn reindex(&mut self, elem: &T) {
        if let Some(idx) = self.reverse_index.find(elem) {
            self.sink(idx);
            self.swim(idx);
        }
    }
}
impl<'a, T, Order: HeapOrder, Cmp: Compare<T, T>, RI: CheckReverseIndex<T>>
    BinaryHeapCmp<T, Cmp, RI, Order>
{
    pub fn push_once(&mut self, elem: T) -> Option<T> {
        if self.reverse_index.check(&elem) {
            return Some(elem);
        } else {
            self.push(elem);
            return None;
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate rand;
    extern crate std;

    use self::rand::{thread_rng, Rng};
    use self::std::prelude::v1::*;
    use super::Natural;
    use util::heap_order::typenum::U8;
    use util::heap_order::BHeapOrder;
    use util::reverse_index::{HashRI, NoRI};
    type Heap = super::BinaryHeapCmp<u64, Natural<u64>, NoRI<u64>, BHeapOrder<U8>>;
    type RIHeap = super::BinaryHeapCmp<u64, Natural<u64>, HashRI<u64>, BHeapOrder<U8>>;

    #[test]
    fn test_basic() {
        let mut rng = thread_rng();
        for n in 0..10 {
            for l in 10..16 {
                let heap = rng.gen_iter::<u64>()
                    .map(|x| x % (1 << l))
                    .take(1 << n)
                    .collect::<Heap>();
                assert!(heap.verify_integrity());
            }
        }
    }
    #[test]
    fn test_rebuild() {
        fn shift(v: u64, l: u64) -> u64 {
            let shift = 1 << l;
            shift - v
        }
        let mut rng = thread_rng();
        for n in 1..10 {
            for l in 10..16 {
                let mut heap = rng.gen_iter::<u64>()
                    .map(|x| x % (1 << l))
                    .take(1 << n)
                    .collect::<Heap>();
                assert!(heap.verify_integrity());
                for i in 0..heap.len() {
                    let v = *heap.peek_idx(i).unwrap();
                    unsafe { *heap.peek_idx_mut(i).unwrap() = shift(v, l) };
                }
                assert!(!heap.verify_integrity());
                unsafe { heap.rebuild() };
                assert!(heap.verify_integrity());
            }
        }
    }
    fn random_unique_vec<T: Rng>(rng: &mut T, size: usize) -> Vec<u64> {
        let mut vec = rng.gen_iter::<u64>().take(size).collect::<Vec<u64>>();
        vec.sort();
        vec.dedup();
        rng.shuffle(&mut vec);
        vec
    }
    #[test]
    fn test_reverse_index() {
        let mut rng = thread_rng();
        for n in 5..10 {
            let size = 1 << n;
            for l in 10..16 {
                let mut heap = RIHeap::new_ri(HashRI::default());
                assert_eq!(heap.get_reverse_index().len(), 0);
                let vec = random_unique_vec(&mut rng, size);
                let size = vec.len();
                heap.extend(vec);
                assert_eq!(heap.get_reverse_index().len(), size);
                heap.pop();
                heap.pop();
                assert_eq!(heap.get_reverse_index().len(), size - 2);
            }
        }
    }
}
