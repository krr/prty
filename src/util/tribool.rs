use std::ops::{BitAnd, BitOr, BitXor, Not};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum TriBool {
    True,
    False,
    Unknown,
}

impl From<bool> for TriBool {
    fn from(b: bool) -> Self {
        if b {
            TriBool::True
        } else {
            TriBool::False
        }
    }
}
impl<T: Into<TriBool>> From<Option<T>> for TriBool {
    fn from(o: Option<T>) -> Self {
        if let Some(t) = o {
            t.into()
        } else {
            TriBool::Unknown
        }
    }
}

impl Not for TriBool {
    type Output = TriBool;
    fn not(self) -> TriBool {
        match self {
            TriBool::True => TriBool::False,
            TriBool::False => TriBool::True,
            _ => TriBool::Unknown,
        }
    }
}
impl BitAnd for TriBool {
    type Output = TriBool;
    fn bitand(self, rhs: TriBool) -> TriBool {
        match (self, rhs) {
            (TriBool::True, x) => x,
            (TriBool::False, _) => TriBool::False,
            (TriBool::Unknown, TriBool::False) => TriBool::False,
            (TriBool::Unknown, TriBool::True) => TriBool::Unknown,
            (TriBool::Unknown, TriBool::Unknown) => TriBool::Unknown,
        }
    }
}
impl BitOr for TriBool {
    type Output = TriBool;
    fn bitor(self, rhs: TriBool) -> TriBool {
        match (self, rhs) {
            (TriBool::False, x) => x,
            (TriBool::True, _) => TriBool::True,
            (TriBool::Unknown, TriBool::True) => TriBool::True,
            (TriBool::Unknown, TriBool::False) => TriBool::Unknown,
            (TriBool::Unknown, TriBool::Unknown) => TriBool::Unknown,
        }
    }
}

impl BitXor for TriBool {
    type Output = TriBool;
    fn bitxor(self, rhs: TriBool) -> TriBool {
        match (self, rhs) {
            (TriBool::False, x) => x,
            (TriBool::True, x) => !x,
            (TriBool::Unknown, _) => TriBool::Unknown,
        }
    }
}
impl BitXor<bool> for TriBool {
    type Output = TriBool;
    fn bitxor(self, rhs: bool) -> TriBool {
        match (self, rhs) {
            (x, false) => x,
            (x, true) => !x,
        }
    }
}
use std::fmt;
pub struct TFU(pub TriBool);
impl fmt::Debug for TFU {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::TriBool::*;
        match self.0 {
            True => write!(f, "T"),
            False => write!(f, "F"),
            Unknown => write!(f, "U"),
        }
    }
}
