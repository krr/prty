use std::fmt;
use std::ops::{BitAnd, BitOr, BitXor, BitXorAssign, Not};

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Sign {
    Pos,
    Neg,
}
use self::Sign::*;

impl Sign {
    pub fn negate_if(&self, b: bool) -> Sign {
        self.clone() ^ b
    }
    pub fn value(&self) -> usize {
        match *self {
            Sign::Pos => 0,
            Sign::Neg => 1,
        }
    }
    pub fn from_value(i: usize) -> Sign {
        match i & 1 {
            0 => Sign::Pos,
            _ => Sign::Neg,
        }
    }
}
impl From<bool> for Sign {
    fn from(b: bool) -> Sign {
        if b {
            Neg
        } else {
            Pos
        }
    }
}
impl From<Sign> for bool {
    fn from(s: Sign) -> bool {
        s == Sign::Neg
    }
}
impl BitAnd for Sign {
    type Output = Sign;
    fn bitand(self, s: Sign) -> Self::Output {
        match (self, s) {
            (Pos, Pos) => Pos,
            _ => Neg,
        }
    }
}
impl BitOr for Sign {
    type Output = Sign;
    fn bitor(self, s: Sign) -> Self::Output {
        match (self, s) {
            (Neg, Neg) => Neg,
            _ => Pos,
        }
    }
}
impl BitXor for Sign {
    type Output = Sign;
    fn bitxor(self, s: Sign) -> Self::Output {
        match (self, s) {
            (Neg, Pos) => Neg,
            (Pos, Neg) => Neg,
            _ => Pos,
        }
    }
}
impl BitXorAssign<bool> for Sign {
    fn bitxor_assign(&mut self, s: bool) {
        *self = *self ^ s;
    }
}
impl BitXor<bool> for Sign {
    type Output = Sign;
    fn bitxor(self, s: bool) -> Self::Output {
        let s: Sign = From::from(s);
        self ^ s
    }
}
impl Not for Sign {
    type Output = Sign;
    fn not(self) -> Self::Output {
        match self {
            Pos => Neg,
            Neg => Pos,
        }
    }
}
impl fmt::Debug for Sign {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Pos => write!(f, "+"),
            Neg => write!(f, "-"),
        }
    }
}

use util::tribool::TriBool;

impl From<Sign> for TriBool {
    fn from(s: Sign) -> TriBool {
        match s {
            Sign::Pos => TriBool::True,
            Sign::Neg => TriBool::False,
        }
    }
}

impl BitXor<Sign> for TriBool {
    type Output = TriBool;
    fn bitxor(self, rhs: Sign) -> TriBool {
        self ^ TriBool::from(rhs)
    }
}
impl PartialOrd for Sign {
    fn partial_cmp(&self, r: &Sign) -> Option<::std::cmp::Ordering> {
        bool::from(*self).partial_cmp(&bool::from(*r))
    }
}
impl Ord for Sign {
    fn cmp(&self, r: &Sign) -> ::std::cmp::Ordering {
        bool::from(*self).cmp(&bool::from(*r))
    }
}
#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn bijective_from() {
        assert_eq!(true, bool::from(Sign::from(true)));
        assert_eq!(false, bool::from(Sign::from(false)));
        assert_eq!(0, Sign::from_value(0).value());
        assert_eq!(1, Sign::from_value(1).value());
    }
}
