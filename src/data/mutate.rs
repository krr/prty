use super::nested_definition::AtomsOf;
use super::*;
use util::permutation::Permutation;
use util::vec::*;

pub trait Permute {
    fn permute(&self, v: &Vec<usize>) -> Self;
}
impl Permute for Atom {
    fn permute(&self, v: &Vec<usize>) -> Atom {
        Atom::new(v[self.idx()])
    }
}
impl Permute for Fact {
    fn permute(&self, v: &Vec<usize>) -> Fact {
        Fact::new(self.sign(), self.atom().permute(v))
    }
}
impl Permute for Body {
    fn permute(&self, v: &Vec<usize>) -> Body {
        self.map_factlist(|facts| facts.permute(v))
    }
}
impl Permute for Rule {
    fn permute(&self, v: &Vec<usize>) -> Rule {
        Rule {
            body: self.body.permute(v),
            def: self.def,
        }
    }
}
impl Permute for AtomsOf {
    fn permute(&self, v: &Vec<usize>) -> AtomsOf {
        AtomsOf::Unsorted(self.as_vec().permute(v))
    }
}
impl<T: Permute> Permute for Vec<T> {
    fn permute(&self, v: &Vec<usize>) -> Vec<T> {
        self.iter().map(|f| f.permute(v)).collect::<Vec<_>>()
    }
}

impl NestedDefinition {
    pub fn add_definition(&mut self, parent: DefIdx, sign: Sign) -> Result<DefIdx, VerifyError> {
        if self.parent.len() != self.signs.len() {
            return Err(VerifyError::IndexOutOfBounds);
        }
        let newdef = DefIdx::new(self.parent.len());
        self.parent.push(parent);
        self.signs.push(sign);
        Ok(newdef)
    }
    pub unsafe fn set_rule(&mut self, a: Atom, rule: Rule) -> Rule {
        set_at(&mut self.rules, a.idx(), rule)
    }
    pub fn fix_atoms_of_def(&mut self) {
        let nb_defs = self.nb_definitions();
        let mut o: Vec<Vec<Atom>> = ::util::vec::vec_default(nb_defs);
        for atom in self.atoms() {
            let loc = atom.loc(self).i();
            o[loc].push(atom);
        }
        self.atoms_of = AtomsOf::Unsorted(o)
    }
    pub fn fix_open(&self) -> Self {
        let rules = self.rules
            .iter()
            .map(|r| {
                if r.loc(self).is_open() {
                    Rule {
                        body: Body::Open,
                        def: r.def,
                    }
                } else {
                    r.clone()
                }
            })
            .collect::<Vec<_>>();
        NestedDefinition {
            rules: rules,
            parent: self.parent.clone(),
            signs: self.signs.clone(),
            atoms_of: self.atoms_of.clone(),
        }
    }
    fn fix_factlist(&self, sign: Sign, v: &[Fact]) -> Vec<Fact> {
        v.iter().map(|f| Fact::new(self.sign(&f.atom()) ^ sign, f.atom())).collect()
    }
    fn fix_rule_signs(&self, r: &Rule) -> Rule {
        let sign = self.def_sign(&r.def);
        let body = r.body.map_factlist(|v| self.fix_factlist(sign, v));
        Rule { body, def: r.def }
    }
    pub fn fix_signs(&self) -> Self {
        let rules = self.rules.iter().map(|r| self.fix_rule_signs(r)).collect::<Vec<_>>();
        NestedDefinition {
            rules: rules,
            parent: self.parent.clone(),
            signs: self.signs.clone(),
            atoms_of: self.atoms_of.clone(),
        }
    }
    pub fn fix_sort(&self) -> (NestedDefinition, Permutation) {
        let mut rules = self.rules.iter().enumerate().map(|(i, r)| (i, r.def)).collect::<Vec<_>>();
        rules.sort_by_key(|&(_, def)| def);
        let mut permute = vec![0; rules.len()];
        for (idx, &(org, _)) in rules.iter().enumerate() {
            permute[org] = idx;
        }
        let rules = rules
            .iter()
            .map(|&(orig_idx, _)| self.rules[orig_idx].permute(&permute))
            .collect::<Vec<_>>();
        let def = NestedDefinition {
            rules,
            parent: self.parent.clone(),
            signs: self.signs.clone(),
            atoms_of: self.atoms_of.permute(&permute),
        };
        (def, Permutation::from(permute))
    }
    pub fn atoms_of_sorted(&mut self) -> bool {
        let mut vec = Vec::<Atom>::new();
        let mut first = 0;
        vec.push(first.into());
        for atoms in match self.atoms_of {
            AtomsOf::Sorted(_) => {
                return false;
            }
            AtomsOf::Unsorted(ref v) => v,
        }.iter()
        {
            for atom in atoms.iter() {
                if atom.idx() != first {
                    return false;
                }
                first += 1;
            }
            vec.push(first.into());
        }
        assert!(vec.len() == self.nb_definitions() + 1);
        assert!(vec.last().unwrap().idx() == self.nb_atoms());
        self.atoms_of = AtomsOf::Sorted(vec);
        return true;
    }

    pub fn sort_body_by_depth(&mut self, order: SortOrder) {
        for body in self.rules.iter_mut().map(|r| &mut r.body) {
            if let Some(v) = match *body {
                Body::And(ref mut v) => Some(v),
                Body::Or(ref mut v) => Some(v),
                _ => None,
            } {
                if order != SortOrder::None {
                    v.sort()
                }
                if order == SortOrder::Desc {
                    v.reverse()
                }
            }
        }
    }
}
#[derive(PrefixOpt, Clone, Copy, Eq, PartialEq)]
pub enum SortOrder {
    None,
    Asc,
    Desc,
}

impl Default for SortOrder {
    fn default() -> Self {
        SortOrder::None
    }
}
