pub mod names;
mod simple;
pub mod interpretation;
mod locate;
pub mod mutate;
pub mod nested_definition;
pub mod parse;

use self::interpretation::Inter;
pub use self::locate::*;
pub use self::nested_definition::NestedDefinition;
pub use self::simple::*;
pub use error::VerifyError;
pub use util::sign::Sign;

#[derive(Clone, Debug)]
pub struct DefInter(pub NestedDefinition, pub Inter);
impl DefInter {
    pub fn verify(&self) -> Result<(), VerifyError> {
        if self.0.nb_atoms() != self.1.len() {
            return Err(VerifyError::IndexOutOfBounds);
        }
        self.0.verify()
    }
}
impl DefInter {
    pub fn fix(&self) -> DefInter {
        let def = self.0.fix_open();
        let def = def.fix_signs();
        //let def = def.fix_sort();
        let inter = self.1.fix_length(def.nb_atoms());
        DefInter(def, inter)
    }
}
