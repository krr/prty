use super::Atom;
use std::borrow::Cow;
use std::collections::HashMap;

pub trait Namer {
    fn get_name(&self, &Atom) -> Cow<str>;
}

impl Namer for () {
    fn get_name(&self, a: &Atom) -> Cow<str> {
        use std::string::ToString;
        Cow::Owned(a.idx().to_string())
    }
}
pub type SimpleNames = HashMap<Atom, String>;
impl Namer for SimpleNames {
    fn get_name(&self, n: &Atom) -> Cow<str> {
        Cow::Borrowed(self.get(n).map(|s| &**s).unwrap_or(""))
    }
}

#[derive(Debug, Clone)]
pub struct InvertPermuteNamer<T>(pub Vec<Atom>, pub T);
impl<T> InvertPermuteNamer<T> {
    fn invert_atom(&self, a: &Atom) -> Atom {
        self.0[a.idx()]
    }
}
impl<T: Namer> Namer for InvertPermuteNamer<T> {
    fn get_name(&self, a: &Atom) -> Cow<str> {
        self.1.get_name(&self.invert_atom(a))
    }
}
