use super::*;
use std::borrow::Cow;
use util::bitvec::BitVec;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum AtomsOf {
    Sorted(Vec<Atom>),
    Unsorted(Vec<Vec<Atom>>),
}

impl Default for AtomsOf {
    fn default() -> AtomsOf {
        AtomsOf::Unsorted(vec![])
    }
}

impl AtomsOf {
    pub fn set_all(&self, def_idx: DefIdx, bv: &mut BitVec) {
        match self {
            AtomsOf::Sorted(ref v) => {
                let from = v[def_idx.i()].idx();
                let to = v[def_idx.i() + 1].idx();
                bv.set_all(from, to);
            }
            AtomsOf::Unsorted(ref v) => {
                for a in &v[def_idx.i()] {
                    bv.set(a.idx(), true);
                }
            }
        }
    }
    pub fn contains(&self, def_idx: DefIdx, a: &Atom) -> bool {
        match self {
            AtomsOf::Sorted(ref v) => {
                let from = v[def_idx.i()].idx();
                let to = v[def_idx.i() + 1].idx();
                from <= a.idx() && a.idx() < to
            }
            AtomsOf::Unsorted(ref v) => v[def_idx.i()].contains(a),
        }
    }
    pub fn as_vec(&self) -> Cow<Vec<Vec<Atom>>> {
        match self {
            AtomsOf::Sorted(ref v) => {
                let out = v.windows(2)
                    .map(|x| (x[0].idx()..x[1].idx()).into_iter().map(From::from).collect::<Vec<_>>())
                    .collect::<Vec<_>>();
                Cow::Owned(out)
            }
            AtomsOf::Unsorted(ref v) => Cow::Borrowed(&v),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct NestedDefinition {
    pub rules: Vec<Rule>,
    pub parent: Vec<DefIdx>,
    pub signs: Vec<Sign>,
    pub atoms_of: AtomsOf,
}
impl NestedDefinition {
    pub fn top(&self) -> DefIdx {
        DefIdx::new(1)
    }
    pub fn nb_definitions(&self) -> usize {
        self.parent.len()
    }
    pub fn rule(&self, a: &Atom) -> &Body {
        &self.rules[a.idx()].body
    }
    pub fn def(&self, a: &Atom) -> DefIdx {
        self.rules[a.idx()].def
    }
    pub fn sign(&self, a: &Atom) -> Sign {
        self.signs[self.def(a).i()]
    }
    pub fn def_sign(&self, d: &DefIdx) -> Sign {
        self.signs[d.i()]
    }
    pub fn nb_atoms(&self) -> usize {
        self.rules.len()
    }
    pub fn nb_opens(&self) -> usize {
        self.atoms().filter(|a| self.def(a).is_open()).count()
    }
    pub fn nb_def_atoms(&self) -> usize {
        self.nb_atoms() - self.nb_opens()
    }
    pub fn atoms(&self) -> ::std::iter::Map<::std::ops::Range<usize>, fn(usize) -> Atom> {
        (0..self.rules.len()).map(Atom::new)
    }
    pub fn parent(&self, idx: DefIdx) -> DefIdx {
        self.parent[idx.i()]
    }
    pub fn verify(&self) -> Result<(), VerifyError> {
        fn check_body(s: &NestedDefinition, d: DefIdx, body: &Body) -> Result<(), VerifyError> {
            let def_sign = s.def_sign(&d);
            for f in body.facts() {
                let atom_sign = s.sign(&f.atom());
                if !f.loc(s).is_open() && def_sign ^ atom_sign != f.sign() {
                    return Err(VerifyError::BadSign);
                }
                if !s.related_to(d, f) {
                    return Err(VerifyError::NotAncestor);
                }
            }
            return Ok(());
        }
        if self.signs.len() != self.parent.len() {
            return Err(VerifyError::IndexOutOfBounds);
        }
        for (i, def) in self.parent.iter().enumerate() {
            if i != 0 && def.i() > i {
                return Err(VerifyError::CyclicNesting);
            } else if i == 0 && def.i() != 0 {
                return Err(VerifyError::OpenBadEnd);
            }
        }
        let mut prev_loc = DefIdx::open();
        for (idx, rule) in self.rules.iter().enumerate() {
            if rule.def.is_open() != rule.body.is_open() {
                return Err(VerifyError::OpenIsDefined);
            } else if prev_loc > rule.def {
                //return Err(VerifyError::Unsorted);
            } else if rule.def.i() >= self.parent.len() {
                return Err(VerifyError::IndexOutOfBounds);
            } else if !self.atoms_of.contains(rule.def, &Atom::new(idx)) {
                return Err(VerifyError::AtomsOfDefWrong);
            }
            check_body(self, rule.def, &rule.body)?;
            prev_loc = rule.def;
        }
        for (idx, atoms) in self.atoms_of.as_vec().iter().enumerate() {
            for atom in atoms {
                if atom.loc(self) != DefIdx::new(idx) {
                    return Err(VerifyError::AtomsOfDefWrong);
                }
            }
        }
        return Ok(());
    }
    pub fn path(&self, mut idx: DefIdx) -> Vec<DefIdx> {
        let mut o = vec![idx];
        while !idx.is_open() {
            idx = self.parent(idx);
            o.push(idx);
        }
        o.reverse();
        o
    }
}
pub struct BasicStat;
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct Stat {
    rules: usize,
    opens: usize,
    facts: usize,
    definitions: usize,
}

mod fmt {
    use super::*;
    use prty_fmtmodifier::*;
    use std::fmt::*;

    impl Format<NestedDefinition> for BasicStat {
        fn fmt(&self, def: &NestedDefinition, f: &mut Formatter) -> Result {
            let rules = def.nb_def_atoms();
            let opens = def.nb_opens();
            let facts = def.rules.iter().flat_map(|r| r.body.facts()).count();
            let definitions = def.nb_definitions();
            Stat {
                rules,
                opens,
                facts,
                definitions,
            }.fmt(f)
        }
    }
}
impl Default for NestedDefinition {
    fn default() -> NestedDefinition {
        NestedDefinition {
            rules: vec![],
            parent: vec![DefIdx::open()],
            signs: vec![Sign::Pos],
            atoms_of: AtomsOf::default(),
        }
    }
}
