use data::*;
use data::{DefInter, interpretation::Inter};
use quickcheck::{Arbitrary, Gen};
use util::sign::Sign;

impl Arbitrary for Atom {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        Atom(u32::arbitrary(g))
    }
    fn shrink(&self) -> Box<Iterator<Item = Self>> {
        Box::new(self.0.shrink().map(|x| Atom(x)))
    }
}
impl Arbitrary for Sign {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        bool::arbitrary(g).into()
    }
    fn shrink(&self) -> Box<Iterator<Item = Self>> {
        Box::new(bool::from(*self).shrink().map(Into::into))
    }
}
impl Arbitrary for Fact {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        <(Sign, Atom)>::arbitrary(g).into()
    }
    fn shrink(&self) -> Box<Iterator<Item = Self>> {
        Box::new(<(Sign, Atom)>::from(*self).shrink().map(Into::into))
    }
}
impl Arbitrary for Body {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let b = bool::arbitrary(g);
        let v = Vec::<Fact>::arbitrary(g);
        if b {
            Body::Or(v)
        } else {
            Body::And(v)
        }
    }
    fn shrink(&self) -> Box<Iterator<Item = Self>> {
        match *self {
            Body::And(ref v) => Box::new(v.shrink().map(|v| Body::And(v))),
            Body::Or(ref v) => Box::new(v.shrink().map(|v| Body::Or(v))),
            Body::Open => ::quickcheck::single_shrinker(Body::Open),
        }
    }
}
impl DefIdx {
    fn decrement_if(&self, def: DefIdx) -> Option<DefIdx> {
        if *self > def {
            Some(DefIdx(self.0 - 1))
        } else if *self == def {
            None
        } else {
            Some(*self)
        }
    }
    fn decrement_merge_parent(&self, def: DefIdx, parent: DefIdx) -> DefIdx {
        self.decrement_if(def).unwrap_or(parent)
    }
}
impl Body {
    fn arbitary_signed_def<G: Gen, F>(g: &mut G, atom: Atom, atoms: usize, parent: &Vec<DefIdx>, signs: &Vec<Sign>, f: F) -> Body
    where
        F: Fn(Atom) -> DefIdx,
    {
        use rand::seq::sample_iter;
        let atoms_in_body = g.size() / 20;
        let idx = f(atom);
        let s = signs[idx.0];
        let mut facts = (0..atoms)
            .into_iter()
            .map(|idx| Atom::new(idx))
            .filter(|a| parent.related_to(f(*a), idx))
            .map(|a| {
                let s = s ^ signs[f(a).0];
                Fact::new(s, a)
            })
            .collect::<Vec<_>>();
        facts.sort();
        facts.dedup();
        let sample = sample_iter(g, facts, atoms_in_body);
        let facts = match sample {
            Ok(v) | Err(v) => v,
        };
        if bool::arbitrary(g) {
            Body::Or(facts)
        } else {
            Body::And(facts)
        }
    }
    fn delete_atom(&self, a: Atom) -> Body {
        let mut cl = self.clone();
        match cl {
            Body::Or(ref mut v) | Body::And(ref mut v) => {
                use data::{Atom, Fact};
                v.retain(|x| x.atom() != a);
                v.iter_mut().for_each(|x| {
                    if x.atom() > a {
                        *x = Fact::new(x.sign(), Atom(x.atom().0 - 1))
                    }
                })
            }
            _ => (),
        };
        cl
    }
}
impl Rule {
    fn decrement_if(&self, def: DefIdx) -> Option<Rule> {
        self.def.decrement_if(def).map(|idx| Rule {
            body: self.body.clone(),
            def: idx,
        })
    }
    fn delete_atom(&self, a: Atom) -> Rule {
        Rule {
            body: self.body.delete_atom(a),
            def: self.def,
        }
    }
}
impl Arbitrary for NestedDefinition {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let size = (g.size() as f64 / 2.).sqrt() as usize;
        let atoms_per_def = g.size() / 10;
        let total_atoms = atoms_per_def * size;
        let parent = (0..size + 1)
            .into_iter()
            .map(|x| DefIdx(if x < 2 { 0 } else { g.gen_range(1, x) }))
            .collect::<Vec<_>>();
        let signs = (0..size + 1).into_iter().map(|_| Sign::arbitrary(g)).collect::<Vec<_>>();
        let rules = {
            let make_rule = |a: usize| -> Rule {
                let atom_to_def = |a: Atom| DefIdx(a.idx() / atoms_per_def);
                let a = Atom::new(a);
                let def_idx = atom_to_def(a);
                Rule {
                    body: if def_idx.is_open() {
                        Body::Open
                    } else {
                        Body::arbitary_signed_def(g, a, total_atoms, &parent, &signs, atom_to_def)
                    },
                    def: def_idx,
                }
            };
            (0..(total_atoms)).into_iter().map(make_rule).collect::<Vec<_>>()
        };
        let mut def = NestedDefinition {
            rules,
            parent,
            signs,
            atoms_of: AtomsOf::Unsorted(vec![]),
        };
        def.fix_atoms_of_def();
        def
    }
    fn shrink(&self) -> Box<Iterator<Item = Self>> {
        Box::new(DefInter(self.clone(), Inter::new(self.nb_atoms())).shrink().map(|di| di.0))
    }
}

impl Arbitrary for DefInter {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let def = NestedDefinition::arbitrary(g);
        let mut interpretation = Inter::new(def.nb_atoms());
        for a in def.atoms().filter(|a| def.rule(a).is_open()) {
            interpretation.set(&a, bool::arbitrary(g));
        }
        DefInter(def, interpretation)
    }
    fn shrink(&self) -> Box<Iterator<Item = Self>> {
        fn del_def(di: &DefInter, idx: DefIdx) -> Option<DefInter> {
            let def = &di.0;
            let mut rules = Vec::with_capacity(def.rules.len());
            for r in &def.rules {
                if let Some(r) = r.decrement_if(idx) {
                    rules.push(r);
                } else {
                    return None;
                }
            }
            let def_parent = def.parent(idx);
            let mut parent = def.parent
                .iter()
                .map(|i| i.decrement_merge_parent(idx, def_parent))
                .collect::<Vec<_>>();
            let mut signs = def.signs.clone();
            signs.remove(idx.0);
            parent.remove(idx.0);
            let mut def = NestedDefinition {
                rules,
                parent,
                signs,
                atoms_of: vec![],
            };
            def.fix_atoms_of_def();
            Some(DefInter(def, di.1.clone()))
        }
        fn del_atom(di: &DefInter, a: Atom) -> DefInter {
            let def = &di.0;
            let mut rules = def.rules.iter().map(|r| r.delete_atom(a)).collect::<Vec<_>>();
            rules.remove(a.idx());
            let mut def = NestedDefinition {
                rules,
                parent: def.parent.clone(),
                signs: def.signs.clone(),
                atoms_of: vec![],
            };
            def.fix_atoms_of_def();
            DefInter(def, di.1.remove(a))
        }
        let rc = ::std::rc::Rc::new(self.clone());
        let shrinked: Vec<Box<Iterator<Item = Self>>> = vec![
            Box::new((2..self.0.nb_definitions()).into_iter().filter_map({
                let rc = rc.clone();
                move |x| del_def(&rc, DefIdx(x))
            })),
            Box::new(self.0.atoms().map({
                let rc = rc.clone();
                move |x| del_atom(&rc, x)
            })),
        ];
        Box::new(shrinked.into_iter().flat_map(|it| it))
    }
}
