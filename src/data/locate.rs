use super::*;

pub trait Loc<T> {
    fn loc(&self, t: &T) -> DefIdx;
}
impl<T> Loc<T> for DefIdx {
    fn loc(&self, _: &T) -> DefIdx {
        *self
    }
}
impl<T> Loc<T> for Rule {
    fn loc(&self, _: &T) -> DefIdx {
        self.def
    }
}
impl Loc<NestedDefinition> for Atom {
    fn loc(&self, d: &NestedDefinition) -> DefIdx {
        d.rules[self.idx()].loc(d)
    }
}
impl Loc<NestedDefinition> for Fact {
    fn loc(&self, d: &NestedDefinition) -> DefIdx {
        self.atom().loc(d)
    }
}
impl<'a, Unref: Loc<T>, T> Loc<T> for &'a Unref {
    fn loc(&self, d: &T) -> DefIdx {
        (*self).loc(d)
    }
}
pub trait NestingRelation {
    fn parent(&self, idx: DefIdx) -> DefIdx;
    fn sub_of<S: Loc<Self>, P: Loc<Self>>(&self, sub: S, parent: P) -> bool
    where
        Self: Sized,
    {
        let sub = sub.loc(self);
        let parent = parent.loc(self);
        sub > parent
    }
    fn related_to<L: Loc<Self>, R: Loc<Self>>(&self, l: L, r: R) -> bool
    where
        Self: Sized,
    {
        let l_loc = l.loc(self);
        let r_loc = r.loc(self);
        if l_loc == r_loc {
            return true;
        }
        self.sub_of(l_loc.max(r_loc), l_loc.min(r_loc))
    }
}
impl NestingRelation for NestedDefinition {
    fn parent(&self, idx: DefIdx) -> DefIdx {
        self.parent[idx.i()]
    }
}
impl<'a, T: NestingRelation> NestingRelation for &'a T {
    fn parent(&self, idx: DefIdx) -> DefIdx {
        (**self).parent(idx)
    }
}
impl NestingRelation for Vec<DefIdx> {
    fn parent(&self, idx: DefIdx) -> DefIdx {
        self[idx.i()]
    }
}
