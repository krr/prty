use bit_set::BitSet;
use data::{Atom, Body, Fact, Rule};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Inter(BitSet);
impl Eval<Atom> for Inter {
    type W = ();
    fn eval(&self, a: &Atom) -> Witness<Self::W> {
        Witness::new(self.0.contains(a.idx()), ())
    }
}
#[derive(Debug)]
pub struct Witness<W>(pub bool, pub W);

pub trait Eval<T> {
    type W;
    fn eval(&self, t: &T) -> Witness<Self::W>;
}
impl<'a, T: Eval<Atom>> Eval<Atom> for &'a T {
    type W = T::W;
    fn eval(&self, a: &Atom) -> Witness<Self::W> {
        (*self).eval(a)
    }
}
impl<T: Eval<Atom>> Eval<Fact> for T {
    type W = <T as Eval<Atom>>::W;
    fn eval(&self, f: &Fact) -> Witness<Self::W> {
        self.eval(&f.atom()) ^ bool::from(f.sign())
    }
}
impl<T: Eval<Fact>> Eval<Body> for T {
    type W = Vec<Atom>;
    fn eval(&self, f: &Body) -> Witness<Self::W> {
        use self::Body::*;
        match *f {
            Open => panic!(),
            Or(ref facts) => {
                let mut v = Vec::with_capacity(facts.len());
                for f in facts.iter() {
                    if self.eval(f).is_true() {
                        v.clear();
                        v.push(f.atom());
                        return Witness::new(true, v);
                    } else {
                        v.push(f.atom());
                    }
                }
                return Witness::new(false, v);
            }
            And(ref facts) => {
                let mut v = Vec::with_capacity(facts.len());
                for f in facts.iter() {
                    if self.eval(f).is_true() {
                        v.push(f.atom());
                    } else {
                        v.clear();
                        v.push(f.atom());
                        return Witness::new(false, v);
                    }
                }
                return Witness::new(true, v);
            }
        }
    }
}
impl<T: Eval<Body>> Eval<Rule> for T {
    type W = <T as Eval<Body>>::W;
    fn eval(&self, f: &Rule) -> Witness<Self::W> {
        self.eval(&f.body)
    }
}

impl Inter {
    pub fn vec(v: Vec<bool>) -> Inter {
        use std::iter::FromIterator;
        Inter(BitSet::from_iter(v.iter().enumerate().filter(|&(_, y)| *y).map(|(x, _)| x)))
    }
    pub fn new(len: usize) -> Inter {
        Inter(BitSet::with_capacity(len))
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn set(&mut self, a: &Atom, v: bool) -> bool {
        if v {
            !self.0.insert(a.idx())
        } else {
            self.0.remove(a.idx())
        }
    }
    pub fn clear(&mut self) {
        self.0.clear();
    }
    pub fn limit(self, _: usize) -> Self {
        self
    }
    pub fn remove(&self, a: Atom) -> Self {
        let mut v = self.0.clone();
        v.remove(a.idx());
        Inter(v)
    }
    pub fn fix_length(&self, len: usize) -> Self {
        let mut inter = self.clone();
        let cur_len = inter.len();
        inter.0.reserve_len(len - cur_len);
        inter
    }
}

impl<W> Witness<W> {
    pub fn map<F, U>(self, f: F) -> Witness<U>
    where
        F: FnOnce(W) -> U,
    {
        let Witness(x, w) = self;
        Witness(x, f(w))
    }

    pub fn replace<U>(self, by: U) -> Witness<U> {
        Witness(self.0, by)
    }
    pub fn w(&self) -> &W {
        &self.1
    }
    pub fn is_true(&self) -> bool {
        self.0
    }
    pub fn is_false(&self) -> bool {
        !self.0
    }
    pub fn new(is_true: bool, witness: W) -> Witness<W> {
        Witness(is_true, witness)
    }
    pub fn into(self) -> Result<W, W> {
        if self.0 {
            Ok(self.1)
        } else {
            Err(self.1)
        }
    }
}
impl<W> Eq for Witness<W> {}
impl<W> PartialEq for Witness<W> {
    fn eq(&self, w: &Witness<W>) -> bool {
        self.is_true() == w.is_true()
    }
}

impl<W> From<Result<W, W>> for Witness<W> {
    fn from(r: Result<W, W>) -> Witness<W> {
        let ok = r.is_ok();
        let w = match r {
            Ok(w) => w,
            Err(w) => w,
        };
        Witness(ok, w)
    }
}
impl<W> ::std::ops::BitXor<bool> for Witness<W> {
    type Output = Witness<W>;
    fn bitxor(self, b: bool) -> Witness<W> {
        let Witness(x, w) = self;
        Witness(x ^ b, w)
    }
}
