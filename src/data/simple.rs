pub use util::sign::Sign;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Atom(u32);
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Fact {
    sign: Sign,
    atom: Atom,
}
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Body {
    Open,
    Or(Vec<Fact>),
    And(Vec<Fact>),
}
#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct Rule {
    pub body: Body,
    pub def: DefIdx,
}
#[derive(Default, Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub struct DefIdx(u32);
pub struct Pack(pub DefIdx, pub Atom);
impl From<Pack> for u64 {
    fn from(Pack(idx, atom): Pack) -> Self {
        let rev_idx = u32::max_value() - idx.0;
        ((rev_idx as u64) << 32) | (atom.0 as u64)
    }
}
impl From<u64> for Pack {
    fn from(u: u64) -> Self {
        let atom = u as u32;
        let idx = u32::max_value() - ((u >> 32) as u32);
        Pack(DefIdx(idx), Atom(atom))
    }
}
impl From<usize> for Atom {
    fn from(u: usize) -> Atom {
        Atom::new(u)
    }
}
impl<'a> From<&'a usize> for Atom {
    fn from(u: &usize) -> Atom {
        Atom::new(*u)
    }
}
impl From<Atom> for usize {
    fn from(a: Atom) -> usize {
        a.idx()
    }
}
impl<'a> From<&'a Atom> for usize {
    fn from(a: &'a Atom) -> usize {
        a.idx()
    }
}
impl Atom {
    pub fn idx(&self) -> usize {
        self.0 as usize
    }
    pub fn invalid() -> Atom {
        Atom(u32::max_value())
    }
    pub fn new(idx: usize) -> Atom {
        Atom(idx as u32)
    }
    pub fn pos(self) -> Fact {
        Fact::new(Sign::Pos, self)
    }
    pub fn neg(self) -> Fact {
        Fact::new(Sign::Neg, self)
    }
}
impl Fact {
    pub fn new(sign: Sign, atom: Atom) -> Fact {
        Fact { sign, atom }
    }
    pub fn sign(&self) -> Sign {
        self.sign
    }
    pub fn atom(&self) -> Atom {
        self.atom
    }
    pub fn as_atom(self) -> Atom {
        self.atom
    }
}
impl Body {
    pub fn is_open(&self) -> bool {
        (*self) == Body::Open
    }
    pub fn is_or(&self) -> bool {
        match *self {
            Body::Or(_) => true,
            _ => false,
        }
    }
    pub fn contains(&self, f: &Fact) -> bool {
        use self::Body::*;
        match *self {
            Open => false,
            Or(ref v) | And(ref v) => v.contains(&f),
        }
    }
    pub fn facts(&self) -> &[Fact] {
        static EMPTY: &'static [Fact] = &[];
        use self::Body::*;
        match *self {
            Open => EMPTY,
            Or(ref v) | And(ref v) => &v,
        }
    }
    pub fn map_factlist<F>(&self, f: F) -> Body
    where
        F: Fn(&Vec<Fact>) -> Vec<Fact>,
    {
        use self::Body::*;
        match *self {
            Open => Open,
            Or(ref v) => Or(f(v)),
            And(ref v) => And(f(v)),
        }
    }
}
impl Rule {
    pub fn is_open(&self) -> bool {
        self.body.is_open()
    }
}
impl Default for Body {
    fn default() -> Body {
        Body::Open
    }
}
impl DefIdx {
    pub fn open() -> DefIdx {
        DefIdx(0)
    }
    pub fn is_open(&self) -> bool {
        *self == DefIdx::open()
    }
    pub fn i(&self) -> usize {
        self.0 as usize
    }
    pub fn new(idx: usize) -> DefIdx {
        DefIdx(idx as u32)
    }
}

impl ::std::ops::Not for Fact {
    type Output = Fact;
    fn not(mut self) -> Self::Output {
        self.sign ^= true;
        self
    }
}
impl From<(Sign, Atom)> for Fact {
    fn from((s, a): (Sign, Atom)) -> Fact {
        Fact::new(s, a)
    }
}
impl From<Fact> for (Sign, Atom) {
    fn from(f: Fact) -> (Sign, Atom) {
        (f.sign, f.atom)
    }
}
mod fmt {
    use super::*;
    use std::fmt::*;
    impl Display for DefIdx {
        fn fmt(&self, f: &mut Formatter) -> Result {
            write!(f, "{}", self.0)
        }
    }
    impl Debug for Atom {
        fn fmt(&self, f: &mut Formatter) -> Result {
            write!(f, "A{}", self.0)
        }
    }
    impl Debug for Fact {
        fn fmt(&self, f: &mut Formatter) -> Result {
            write!(f, "{:?}{:?}", self.sign, self.atom)
        }
    }
}
