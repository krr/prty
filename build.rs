extern crate peg;
extern crate git_build_version;

const PACKAGE_TOP_DIR : &'static str = ".";

fn main() {
    println!("cargo:rerun-if-changed={}/{}", PACKAGE_TOP_DIR, ".git/refs/heads");
    git_build_version::write_version(PACKAGE_TOP_DIR).expect("Saving git version");
    peg::cargo_build("src/data/parse/nested_def.rustpeg");
    peg::cargo_build("src/data/parse/nested_def_rust.rustpeg");
    peg::cargo_build("src/parity_game/parity_game.rustpeg");
}
