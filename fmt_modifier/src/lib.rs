use std::fmt::*;

pub trait Format<What> {
    fn fmt(&self, w: &What, f: &mut Formatter) -> Result;
}

pub struct PrintAs<G, P>(pub G, pub P);
pub struct PrintRefAs<'a, G: 'a, P>(pub &'a G, pub P);

impl<G, P> Debug for PrintAs<G, P>
where
    P: Format<G>,
{
    fn fmt(&self, f: &mut Formatter) -> Result {
        self.1.fmt(&self.0, f)
    }
}
impl<'a, G, P> Debug for PrintRefAs<'a, G, P>
where
    P: Format<G>,
{
    fn fmt(&self, f: &mut Formatter) -> Result {
        self.1.fmt(self.0, f)
    }
}

impl<G, P: Default> From<G> for PrintAs<G, P> {
    fn from(g: G) -> PrintAs<G, P> {
        PrintAs(g.into(), P::default())
    }
}
impl<G, P> ::std::ops::Deref for PrintAs<G, P> {
    type Target = G;
    fn deref(&self) -> &G {
        &self.0
    }
}

impl<G, P> ::std::ops::DerefMut for PrintAs<G, P> {
    fn deref_mut(&mut self) -> &mut G {
        &mut self.0
    }
}
