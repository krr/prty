use super::Graph;

#[derive(Debug, Clone)]
pub struct EdgeVec(Vec<bool>);
#[derive(Debug, Clone)]
pub struct BitGraph(Vec<EdgeVec>);
impl EdgeVec {
    pub fn iter(&self) -> ::std::slice::Iter<bool> {
        self.0.iter()
    }
    pub fn iter_mut(&mut self) -> ::std::slice::IterMut<bool> {
        self.0.iter_mut()
    }
}
impl Graph for BitGraph {
    type Node = usize;
    fn nb_nodes(&self) -> usize {
        self.0.len()
    }
    fn nodes(&self) -> Box<Iterator<Item = Self::Node>> {
        Box::new((0..self.nb_nodes()).into_iter())
    }
    fn get(&self, from: &Self::Node, to: &Self::Node) -> bool {
        self[*from][*to]
    }
    fn set_edge(&mut self, from: &Self::Node, to: &Self::Node, b: bool) {
        self[*from][*to] = b;
    }
}
impl BitGraph {
    pub fn dependents_mut<'a>(&'a mut self, node: usize) -> Box<'a + Iterator<Item = (usize, &'a mut bool)>> {
        let iter = self[node].iter_mut().enumerate().filter(|&(_, ref b)| **b).map(|(idx, b)| (idx, b));
        Box::new(iter)
    }
}
impl ::std::ops::Index<usize> for BitGraph {
    type Output = EdgeVec;
    fn index(&self, n: usize) -> &Self::Output {
        &self.0[n]
    }
}
impl ::std::ops::IndexMut<usize> for BitGraph {
    fn index_mut(&mut self, n: usize) -> &mut Self::Output {
        &mut self.0[n]
    }
}

impl ::std::ops::Index<usize> for EdgeVec {
    type Output = bool;
    fn index(&self, n: usize) -> &Self::Output {
        &self.0[n]
    }
}
impl ::std::ops::IndexMut<usize> for EdgeVec {
    fn index_mut(&mut self, n: usize) -> &mut Self::Output {
        &mut self.0[n]
    }
}
