use super::Graph;
use std::fmt::*;
extern crate prty_fmtmodifier;
pub use self::prty_fmtmodifier::*;

#[derive(Default, Debug, Clone, Copy)]
pub struct JustPrinter;

impl<G: Graph> Format<G> for JustPrinter
where
    G::Node: Debug,
{
    fn fmt(&self, w: &G, f: &mut Formatter) -> Result {
        for to in w.nodes() {
            write!(f, "{:?} <- ", to)?;
            for from in w.iter_to(to) {
                write!(f, "{:?} & ", from)?;
            }
            write!(f, "; ")?;
        }
        Ok(())
    }
}
