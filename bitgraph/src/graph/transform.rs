use super::sparsegraph::SparseGraph;
use super::Graph;
use std::marker::PhantomData;

#[derive(Debug, Clone)]
pub struct MapGraph<G, T>(G, PhantomData<T>);
pub struct Transpose<G>(G);

impl<G, T> From<G> for MapGraph<G, T> {
    fn from(g: G) -> MapGraph<G, T> {
        MapGraph(g, PhantomData)
    }
}
impl<T> MapGraph<SparseGraph, T>
where
    T: 'static + for<'a> From<&'a usize>,
    usize: for <'a> From<&'a T>,
{
    pub fn iter_from_map_nobox<'a>(&'a self, from: T) -> impl Iterator<Item = T> + 'a {
        self.0.iter_from_nobox(From::from(&from)).map(From::from)
    }
}
impl<G: Graph, T> Graph for MapGraph<G, T>
where
    T: 'static + From<G::Node>,
    G::Node: 'static + for<'a> From<&'a T>,
{
    type Node = T;

    fn nb_nodes(&self) -> usize {
        self.0.nb_nodes()
    }
    fn nodes(&self) -> Box<Iterator<Item = Self::Node>> {
        Box::new(self.0.nodes().map(From::from))
    }
    fn get(&self, from: &Self::Node, to: &Self::Node) -> bool {
        self.0.get(&from.into(), &to.into())
    }
    fn set_edge(&mut self, from: &Self::Node, to: &Self::Node, b: bool) {
        self.0.set_edge(&from.into(), &to.into(), b);
    }

    fn clear_all_to(&mut self, to: &Self::Node) {
        self.0.clear_all_to(&to.into())
    }
    fn add_all_to<I>(&mut self, to: &Self::Node, from: I)
    where
        I: IntoIterator<Item = Self::Node>,
    {
        self.0
            .add_all_to(&to.into(), from.into_iter().map(|n| From::from(&n)))
    }
    fn clear_all_from(&mut self, from: &Self::Node) {
        self.0.clear_all_from(&From::from(from))
    }
    fn add_all_from<I>(&mut self, from: &Self::Node, to: I)
    where
        I: IntoIterator<Item = Self::Node>,
    {
        self.0
            .add_all_from(&from.into(), to.into_iter().map(|n| From::from(&n)))
    }
    fn iter_from<'a>(&'a self, from: Self::Node) -> Box<'a + Iterator<Item = Self::Node>> {
        assert!(false);
        Box::new(self.0.iter_from(From::from(&from)).map(From::from))
    }
    fn iter_to<'a>(&'a self, to: Self::Node) -> Box<'a + Iterator<Item = Self::Node>> {
        Box::new(self.0.iter_to(From::from(&to)).map(From::from))
    }
    fn has_from<'a>(&'a self, from: &Self::Node) -> bool {
        self.0.has_from(&From::from(from))
    }
    fn has_to<'a>(&'a self, to: &Self::Node) -> bool {
        self.0.has_to(&From::from(to))
    }
    fn drain_from_and_clear_all_to_drained<'a>(
        &'a mut self,
        from: Self::Node,
    ) -> Box<'a + Iterator<Item = Self::Node>> {
        Box::new(
            self.0
                .drain_from_and_clear_all_to_drained(From::from(&from))
                .map(From::from),
        )
    }
}

impl<G, T> ::std::ops::Deref for MapGraph<G, T> {
    type Target = G;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<G, T> ::std::ops::DerefMut for MapGraph<G, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl<G: Graph> Graph for Transpose<G> {
    type Node = G::Node;

    fn nb_nodes(&self) -> usize {
        self.0.nb_nodes()
    }
    fn nodes(&self) -> Box<Iterator<Item = Self::Node>> {
        self.0.nodes()
    }
    fn get(&self, from: &Self::Node, to: &Self::Node) -> bool {
        self.0.get(to, from)
    }
    fn set_edge(&mut self, from: &Self::Node, to: &Self::Node, b: bool) {
        self.0.set_edge(to, from, b);
    }

    fn clear_all_to(&mut self, to: &Self::Node) {
        self.0.clear_all_from(to)
    }
    fn add_all_to<I>(&mut self, to: &Self::Node, from: I)
    where
        I: IntoIterator<Item = Self::Node>,
    {
        self.0.add_all_from(to, from)
    }
    fn clear_all_from(&mut self, from: &Self::Node) {
        self.0.clear_all_to(from)
    }
    fn add_all_from<I>(&mut self, from: &Self::Node, to: I)
    where
        I: IntoIterator<Item = Self::Node>,
    {
        self.0.add_all_to(from, to)
    }
    fn iter_from<'a>(&'a self, from: Self::Node) -> Box<'a + Iterator<Item = Self::Node>> {
        self.0.iter_to(from)
    }
    fn iter_to<'a>(&'a self, to: Self::Node) -> Box<'a + Iterator<Item = Self::Node>> {
        self.0.iter_from(to)
    }
}
