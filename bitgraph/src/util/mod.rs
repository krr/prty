
pub trait FinishIteratorExt: Sized + Iterator {
    fn finish(self) -> FinishIterator<Self>;
}
pub struct FinishIterator<T: Iterator> {
    iter: T
}

impl<T: Iterator> FinishIteratorExt for T {
    fn finish(self) -> FinishIterator<T> {
        FinishIterator {iter: self}
    }
}
impl<T: Iterator> Iterator for FinishIterator<T> {
    type Item = T::Item;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
impl<T: Iterator> Drop for FinishIterator<T> {
    fn drop(&mut self) {
        self.for_each(|_| {});
    }
}
