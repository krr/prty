#![cfg_attr(test, feature(plugin))]
extern crate clap;
extern crate quickcheck;

extern crate env_logger;
extern crate log;
#[macro_use]
extern crate measure_time;
extern crate prty;
extern crate prefixopt;
#[macro_use]
extern crate prefixopt_derive;
extern crate rand;

use prty::data::NestedDefinition;
use self::data::interpretation::Inter;
use self::data::DefInter;
use self::prty::*;
use clap::{App, Arg};
use prty::parity_game::ParityGame;
use std::error::Error;
use std::io::{stdin, Read, BufRead, BufReader};
extern crate anycat;
use data::mutate::SortOrder;

#[derive(Default, PrefixOpt, Clone, Copy, Eq, PartialEq)]
pub struct InductionOpt {
    order: SortOrder,
    opt: bool,
}

include!(concat!(env!("OUT_DIR"), "/version.rs"));
pub fn as_read(f: Option<&str>) -> Result<Box<Read>, Box<Error>> {
    Ok(if let Some(file) = f {
        Box::new(anycat::readfile(file))
    } else {
        Box::new(stdin())
    })
}
pub fn run_all_files(stream: &mut Read, o: InductionOpt) -> Result<(), Box<Error>> {
    let stream = BufReader::new(stream);
    for line in stream.lines() {
        let line = line?;
        info!("Running file {:?}", line);
        let mut r = as_read(Some(&line))?;
        run_read(&mut r, o)?
    }
    Ok(())
}
pub fn run_read(stream: &mut Read, o: InductionOpt) -> Result<(), Box<Error>> {
    let game = {
        info_time!("Parsing");
	    ParityGame::parse(&mut ::parity_game::no_pg_parse::Reader::new(stream))
    };
    run_game(game, o);
    Ok(())
}

pub fn run_game(def: NestedDefinition, o: InductionOpt) {
	//    eprintln!("Test: {:?} {:?}", game.nb_nodes(), game.nb_edges());
    let (def, perm) = {
        info_time!("Sorting");
        let (mut def, perm) = def.fix_sort();
        def.sort_body_by_depth(o.order);
        def.atoms_of_sorted();
        let inter = Inter::new(def.nb_atoms());
        let def = DefInter(def, inter);
        (def, perm)
    };
    let just = {
        info_time!("Induction");
        def.induct(o.opt)
    };
    {
        info!("stat, {}", just.stat);
        info_time!("Solution");
        let sol2 = ParityGame::from_mu_state(&def.0, &just, &perm);
        println!("{}", sol2);
    }
}
pub fn run(app: clap::ArgMatches, o: InductionOpt) -> Result<(), Box<Error>> {
    let mut r = as_read(app.value_of("file"))?;
    if app.is_present("list") {
        error!("Running all files");
        run_all_files(&mut r, o)
    } else {
        error!("Running one file");
        run_read(&mut r, o)
    }
}
pub fn main() {
    env_logger::Builder::from_default_env()
        .default_format_timestamp(false)
        .default_format_module_path(false)
        .init();

    let app = App::new("NestedDefintions")
        .version(VERSION)
        .author("Ruben Lapauw")
        .about("Parity game solver by nested inductive definitions")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .help("Parity game file, stdin by default")
                .index(1),
        )
        .arg(
            Arg::with_name("list")
                .short("l")
                .long("list")
                .help("Use file/stdin as a list of files to execute"),
        );
    use prefixopt::*;
    let sort_o = InductionOpt::with_prefix("o");
    let app = sort_o.as_arguments().bind_app(app);
    let app = app.get_matches();
    let a = sort_o.default_parse_args(&app);
    run(app, a.unwrap()).unwrap();
}
